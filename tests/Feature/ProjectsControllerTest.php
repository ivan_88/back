<?php

namespace Tests\Feature;

use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Tests\CreatesApplication;
use Laravel\Passport\Passport;
use App\User;
use Illuminate\Support\Facades\Redis;

class ProjectsControllerTest extends BaseTestCase
{
    use CreatesApplication, Seedable;

    private $user;
    public $baseUrl = 'http://localhost:8000';

    protected function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->user = User::find(1);
        Passport::actingAs($this->user, ['updateProfile']);

        $this->seeding();
    }

    /**
     * Test index
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::index()
     * @return void
     */
    public function testIndex()
    {
        $this->be($this->user);

        $this->get('/api/projects')
            ->seeStatusCode(200)
            ->seeJsonStructure([
                'projects' => ['*' => ['id', 'name_en', 'name_uk', 'created_at', 'updated_at', 'deleted_at']]
            ]);
    }

    /**
     * Test show
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::show()
     * @return void
     */
    public function testShow()
    {
        $this->be($this->user);

        $this->get('/api/projects/show/1')
            ->seeStatusCode(200)
            ->seeJsonStructure(['success', 'project' =>
                ['id', 'name_en', 'name_uk', 'created_at', 'updated_at', 'deleted_at']
            ]);

        $this->get('/api/projects/show/555')
            ->seeStatusCode(404)
            ->seeJson(['errors' => ['404']]);
    }


    /**
     * Test store errors
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::store()
     *
     * @dataProvider storeDataProvider
     */
    public function testStoreFail($data, $error)
    {
        $this->be($this->user);

        $this->post('/api/projects/store', $data)
            ->seeJson(['errors' => $error])
            ->seeStatusCode(400);
    }

    public function storeDataProvider()
    {
        return [
            [
                'No name_en' => [
                    'name_en' => '',
                    'name_uk' => 'Тест 2',
                ], ['name_en' => ['The name en field is required.']]
            ],
            [
                'No name_uk' => [
                    'name_en' => 'test2',
                    'name_uk' => '',
                ], ['name_uk' => ['The name uk field is required.']]
            ]
        ];
    }

    /**
     * Test store errors
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::store()
     */
    public function testStoreSuccess()
    {
        $this->be($this->user);

        $this->post('/api/projects/store', [
            'name_en' => 'Test 6',
            'name_uk' => 'Тест 6'
        ])->seeStatusCode(200)
            ->seeJsonContains(['success' => true])
            ->seeJsonContains([
                'name_en' => 'Test 6',
                'name_uk' => 'Тест 6'
            ])
            ->seeInDatabase('projects', [
                'name_en' => 'Test 6',
                'name_uk' => 'Тест 6'
            ]);
    }

    /**
     * Test update errors
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::update()
     *
     * @dataProvider storeDataProvider
     */
    public function testUpdateFail($data, $error)
    {
        $this->be($this->user);

        $this->post('/api/projects/update/4', $data)
            ->seeJson(['errors' => $error])
            ->seeStatusCode(400);
    }

    /**
     * Test update
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::update()
     */
    public function testUpdate()
    {
        $this->be($this->user);

        $this->post('/api/projects/update/4', [
            'name_en' => 'Test 7',
            'name_uk' => 'Тест 7'
        ])->seeStatusCode(200)
            ->seeJsonContains(['success' => true])
            ->seeJsonContains([
                'name_en' => 'Test 7',
                'name_uk' => 'Тест 7'
            ])
            ->seeInDatabase('projects', [
                'name_en' => 'Test 7',
                'name_uk' => 'Тест 7'
            ]);

        $this->post('/api/projects/update/55', [
            'name_en' => 'Test 7',
            'name_uk' => 'Тест 7'
        ])->seeStatusCode(404)
            ->seeJson(['errors' => ['404']]);
    }

    /**
     * Test delete
     *
     * @covers \app\Http\Controllers\Api\ProjectsController::delete()
     */
    public function testDelete()
    {
        $this->be($this->user);

        $this->get('/api/projects/delete/4')
            ->seeStatusCode(200)
            ->seeJsonContains(['success' => true])
            ->dontSeeInDatabase('projects', ['id' => 4, 'deleted_at' => null]);

        $this->get('/api/projects/delete/55')
            ->seeStatusCode(404)
            ->seeJson(['errors' => ['404']]);
    }

    public static function setUpBeforeClass()
    {
        parent::setUpBeforeClass(); // TODO: Change the autogenerated stub
        Redis::set('seed', 0);
    }
}
