<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\DB;

trait Seedable {

    protected function seeding()
    {
        if (Redis::get('seed') != 1) {
            $this->seed('Project');
            $this->seed('Interests');
            $this->seed('Sources');
            $this->seed('CountriesSeeder');
            Redis::set('seed', 1);
        }
    }
}