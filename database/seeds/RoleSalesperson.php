<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSalesperson extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['id' => Role::SALES, 'name' => 'sales', 'display_name' => 'Sales person', 'Description' => 'This is a sales person']);
    }
}
