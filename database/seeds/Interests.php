<?php

use Illuminate\Database\Seeder;
use App\Interest;

class Interests extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('interests')->truncate();
        Interest::create(['name_en' => 'House', 'name_uk' => 'Будинок', 'project_id' => 1]);
        Interest::create(['name_en' => 'Apartment', 'name_uk' => 'Апартаменти', 'project_id' => 1]);
        Interest::create(['name_en' => 'Parking place', 'name_uk' => 'Парковочне місце', 'project_id' => 1]);
        Interest::create(['name_en' => 'Garage', 'name_uk' => 'Гараж', 'project_id' => 1]);
    }
}
