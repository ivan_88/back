<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(Roles::class);
        $this->call(Sources::class);
        $this->call(RoleSalesperson::class);
        $this->call(RolePartner::class);
    }
}
