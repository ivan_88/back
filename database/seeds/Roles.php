<?php

use Illuminate\Database\Seeder;
use App\Role;

class Roles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['id' => Role::ADMIN, 'name' => 'admin', 'display_name' => 'Admin', 'Description' => 'This is a super user']);
        Role::create(['id' => Role::MANAGER, 'name' => 'manager', 'display_name' => 'Manager', 'Description' => 'Manager can edit brokers and customers']);
        Role::create(['id' => Role::BROKER, 'name' => 'broker', 'display_name' => 'Broker', 'Description' => 'Just can add new lead']);
    }
}
