<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolePartner extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['id' => Role::PARTNER, 'name' => 'partner', 'display_name' => 'Partner', 'Description' => 'This is a partner']);
    }
}
