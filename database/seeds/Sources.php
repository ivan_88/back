<?php

use Illuminate\Database\Seeder;
use App\Source;
use Illuminate\Support\Facades\DB;

class Sources extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sources')->truncate();
        Source::create(['name_en' => 'Lives nearby', 'name_uk' => 'Живе поруч']);
        Source::create(['name_en' => 'By recommendations', 'name_uk' => 'За рекомендацією']);
        Source::create(['name_en' => 'Passed by', 'name_uk' => 'Проходив повз']);
        Source::create(['name_en' => 'Info BM', 'name_uk' => 'Інформація BM']);
        Source::create(['name_en' => 'Ringostat', 'name_uk' => 'Ringostat']);
        Source::create(['name_en' => 'Repeated sale', 'name_uk' => 'Повторний продаж']);
        Source::create(['name_en' => 'Internet', 'name_uk' => 'Інтернет']);
        Source::create(['name_en' => 'Facebook', 'name_uk' => 'Facebook']);
        Source::create(['name_en' => 'Brokers (realty agents, personnel etc)', 'name_uk' => 'Брокери']);
        Source::create(['name_en' => 'My personal actions', 'name_uk' => 'Мої особисті дії']);
        Source::create(['name_en' => 'Outcoming call', 'name_uk' => 'Вихідний дзвінок']);
        Source::create(['name_en' => 'Radio', 'name_uk' => 'Радіо']);
        Source::create(['name_en' => 'Flyers/Direct Mail', 'name_uk' => 'Флаєри / Пряма пошта']);
        Source::create(['name_en' => 'Presentation', 'name_uk' => 'Презентація']);
        Source::create(['name_en' => 'TV', 'name_uk' => 'Телебачення']);
    }
}
