<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrokerUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'vasya',
            'email' => 'vasya@vasya.com',
            'password' => bcrypt('vasya'),
        ]);
        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => 3
        ]);
    }
}
