<?php

use Illuminate\Database\Seeder;

class Project extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->truncate();
        App\Project::create(['name_en' => 'Pustomyty', 'name_uk' => 'Пустомити']);
        App\Project::create(['name_en' => 'Belleville', 'name_uk' => 'Беллевіль']);
        App\Project::create(['name_en' => 'Dzherelna', 'name_uk' => 'Джерельна']);
    }
}
