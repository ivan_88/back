<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = App\User::create([
            'name' => 'admin',
            'email' => 'admin2@admin.com',
            'password' => bcrypt('password'),
        ]);

        DB::table('role_user')->insert([
            'user_id' => $user->id,
            'role_id' => 1
        ]);
    }
}
