<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContentLogAddContentId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents_log', function(Blueprint $table) {
            $table->integer('content_id')->after('user_id');
            $table->index('content_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents_log', function(Blueprint $table) {
            $table->dropIndex('contents_log_content_id_index');
            $table->dropColumn('content_id');
        });
    }
}
