<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterContentsAddContactBy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->tinyInteger('contact_by');
            $table->string('cause_of_recovery')->nullable();
            $table->index('contact_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->dropIndex('contents_contact_by_index');
            $table->dropColumn('contact_by');
            $table->dropColumn('cause_of_recovery');
        });
    }
}
