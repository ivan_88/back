<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterClientsAddUserProject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function(Blueprint $table) {
            $table->integer('user_id')->after('active');
            $table->integer('project_id')->after('user_id');
            $table->index('user_id');
            $table->index('project_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function(Blueprint $table) {
            $table->dropIndex('contents_user_id_index');
            $table->dropIndex('contents_project_id_index');
            $table->dropColumn('user_id');
            $table->dropColumn('project_id');
        });
    }
}
