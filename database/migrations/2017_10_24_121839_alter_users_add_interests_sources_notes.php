<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsersAddInterestsSourcesNotes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->dropColumn('notes');
            $table->integer('interest_id');
            $table->integer('source_id');
            $table->string('rejection_reason')->nullable();
            $table->index('interest_id');
            $table->index('source_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contents', function (Blueprint $table) {
            $table->dropIndex('contents_interest_id_index');
            $table->dropIndex('contents_source_id_index');
            $table->dropColumn('rejection_reason');
            $table->dropColumn('interest_id');
            $table->dropColumn('source_id');
        });
    }
}
