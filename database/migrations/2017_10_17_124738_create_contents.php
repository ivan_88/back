<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function(Blueprint $table) {
            $table->increments('id');
            $table->string('firstname', 32);
            $table->string('lastname', 32);
            $table->string('phone', 11);
            $table->string('email', 64);
            $table->integer('country_id');
            $table->string('city', 32)->nullable()->default(null);
            $table->string('address', 256)->nullable()->default(null);
            $table->string('zip', 5)->nullable()->default(null);
            $table->text('notes')->nullable()->default(null);
            $table->tinyInteger('type');
            $table->tinyInteger('active');
            $table->timestamps();
            $table->index('country_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
}
