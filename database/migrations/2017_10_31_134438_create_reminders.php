<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReminders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reminders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('content_id');
            $table->text('note');
            $table->dateTime('assigned_to');
            $table->tinyInteger('show')->default(1);
            $table->tinyInteger('done')->default(0);
            $table->timestamps();
            $table->index('user_id');
            $table->index('content_id');
            $table->index('show');
            $table->index('done');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reminders');
    }
}
