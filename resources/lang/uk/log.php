<?php

return [
    'create_customer' => 'Створення клієнта',
    'update_customer' => 'Редагування клієнта',
    'archive' => 'Архівування',
    'active' => 'Відновлення',
    'add_note' => 'Створення нової нотатки',
    'lead_to_com' => 'Переміщення LEAD до COM',
    'com_to_neg' => 'Переміщення COM до NEG',
    'neg_to_pro' => 'Переміщення NEG до PRO',
    'pro_to_neg' => 'Переміщення PRO до NEG',
    'neg_to_com' => 'Переміщення NEG до COM',
    'com_to_lead' => 'Переміщення COM до LEAD'
];