<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Атрібут :attribute повинен бути підтверджений.',
    'active_url'           => 'Атрібут :attribute не є валідним URL.',
    'after'                => 'Атрібут :attribute мусить бути датою після :date.',
    'after_or_equal'       => 'Атрібут :attribute мусить бути датою після або дорівнювати даті :date.',
    'alpha'                => 'Атрібут :attribute мусить містити тільки букви.',
    'alpha_dash'           => 'Атрібут :attribute мусить містити тільки букви, числа, і тире.',
    'alpha_num'            => 'Атрібут :attribute мусить містити тільки букви і числа.',
    'array'                => 'Атрібут :attribute масить бути массивом.',
    'before'               => 'Атрібут :attribute мусить бути датою після :date.',
    'before_or_equal'      => 'Атрібут :attribute мусить бути датою після або дорівнювати даті :date.',
    'between'              => [
        'numeric' => 'Атрібут :attribute мусить бути між :min та :max.',
        'file'    => 'Атрібут :attribute мусить бути між :min та :max кілобайт.',
        'string'  => 'Атрібут :attribute мусить бути між :min та :max символів.',
        'array'   => 'Атрібут :attribute мусить мати між :min та :max елементів.',
    ],
    'boolean'              => 'Поле :attribute мусить бути true або false.',
    'confirmed'            => ':attribute підтвердження не відповідає.',
    'date'                 => 'Поле :attribute не є правильною датою.',
    'date_format'          => 'Поле :attribute не вібповідає формату дати :format.',
    'different'            => 'Поле :attribute та :other повинні бути однаковими.',
    'digits'               => 'Поле :attribute повинно бути :digits цифр.',
    'digits_between'       => 'Поле :attribute повинно бути між :min та :max цифр.',
    'dimensions'           => 'Поле :attribute є неправильним розміром зображення.',
    'distinct'             => 'Поле :attribute містить дубльоване значення.',
    'email'                => 'Поле :attribute мусить бути правильною email адресою.',
    'exists'               => 'Вибраний атрібут :attribute не існує.',
    'file'                 => 'Поле :attribute мусить бути файлом.',
    'filled'               => 'Поле :attribute мусить бути заповненим.',
    'image'                => 'Поле :attribute мусить бути зображенням.',
    'in'                   => 'Поле :attribute не існує.',
    'in_array'             => 'Поле :attribute не існує в :other.',
    'integer'              => 'Поле :attribute мусить бути цілим числом.',
    'ip'                   => 'Поле :attribute мусить бути правильною IP адресою.',
    'ipv4'                 => 'Поле :attribute мусить бути правильною IPv4 адресою.',
    'ipv6'                 => 'Поле :attribute мусить бути правильною IPv6 адресою.',
    'json'                 => 'Поле :attribute мусить бути правильною JSON строкою.',
    'max'                  => [
        'numeric' => 'Поле :attribute мусить бути більше :max.',
        'file'    => 'Поле :attribute мусить бути більше :max кілобайт.',
        'string'  => 'Поле :attribute мусить бути більше :max символів.',
        'array'   => 'Поле :attribute мусить бути більше :max елементів.',
    ],
    'mimes'                => ':attribute мусить бути файлом типу: :values.',
    'mimetypes'            => ':attribute  мусить бути файлами типу: :values.',
    'min'                  => [
        'numeric' => 'Поле :attribute мусить бути менше :min.',
        'file'    => 'Поле :attribute мусить бути менше :min кілобайт.',
        'string'  => 'Поле :attribute мусить бути менше :min символів.',
        'array'   => 'Поле :attribute мусить бути менше :min елементів.',
    ],
    'not_in'               => 'Поле :attribute не вірне.',
    'numeric'              => 'Поле :attribute мусить бути числом.',
    'present'              => 'Поле :attribute мусить бути присутнім.',
    'regex'                => 'Поле :attribute має невірний формат.',
    'required'             => 'Поле :attribute є обов\'язковим.',
    'required_if'          => 'Поле :attribute є обов\'язковим коли :other має значення :value.',
    'required_unless'      => 'Поле :attribute є обов\'язковим якщо не :other має значення в межах :values.',
    'required_with'        => 'Поле :attribute є обов\'язковим коли :values присутнє.',
    'required_with_all'    => 'Поле :attribute є обов\'язковим коли :values присутнє.',
    'required_without'     => 'Поле :attribute є обов\'язковим коли :values не присутнє.',
    'required_without_all' => 'Поле :attribute є обов\'язковим коли жодне з :values присутнє.',
    'same'                 => 'Поле :attribute та :other мають бути присутніми.',
    'size'                 => [
        'numeric' => 'Поле :attribute повинні мати розмір :size.',
        'file'    => 'Поле :attribute повинні мати розмір :size кілобайт.',
        'string'  => 'Поле :attribute повинні мати розмір :size символів.',
        'array'   => 'Поле :attribute повинні містити :size елементів.',
    ],
    'string'               => 'Поле :attribute повинно бути строкою.',
    'timezone'             => 'Поле :attribute повинно бути часовою зоною.',
    'unique'               => 'Поле :attribute вже використовується.',
    'uploaded'             => 'Поле :attribute не вдалося завантажити.',
    'url'                  => 'Поле :attribute формат не вірний.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
