<?php

return [
    'create_customer' => 'Create customer',
    'update_customer' => 'Update customer',
    'archive' => 'Archive',
    'active' => 'Active',
    'add_note' => 'Add new note',
    'lead_to_com' => 'Move LEAD to COM',
    'com_to_neg' => 'Move COM to NEG',
    'neg_to_pro' => 'Move NEG to PRO',
    'pro_to_neg' => 'Move PRO to NEG',
    'neg_to_com' => 'Move NEG to COM',
    'com_to_lead' => 'Move COM to LEAD'
];