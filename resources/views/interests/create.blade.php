@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="surf">
                        <h4 class="title">@lang('interface.create_interest')</h4>
                        <p class="category"><a href="/interests" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/interests/store" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group label-floating col-md-3">
                                    <label class="control-label">@lang('interface.name_en')</label>
                                    <input class="form-control" type="text" name="name_en"/>
                                </div>

                                <div class="form-group label-floating col-md-3">
                                    <label class="control-label">@lang('interface.name_uk')</label>
                                    <input class="form-control" type="text" name="name_uk"/>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.project')</label>
                                        <select name="project_id" class="form-control">
                                            @foreach($projects as $project)
                                                <option value="{{$project->id}}">{{$project->$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <input type="submit" value="@lang('interface.create')" class=" submit-btn btn btn-success pull-right"/>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection