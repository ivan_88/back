@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header row" data-background-color="surf">
                        <div class="col-md-6">
                            <h4 class="title">@lang('interface.notes'): <b>{{$content->firstname . ' ' . $content->lastname}}</b></h4>
                            <p class="category"><a href="/" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                        </div>
                        <div class="col-md-6 text-right">

                        </div>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <tr>
                                <th>@lang('interface.text')</th>
                                <th>@lang('interface.user')</th>
                                <th>@lang('interface.date')</th>
                            </tr>
                            @foreach($notes as $note)
                                <tr>
                                    <td>{{ $note->text }}</td>
                                    <td>{{ $note->user->name or 'DELETED' }}</td>
                                    <td>{{ $note->created_at }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                    {{ $notes->links('parts.pagination') }}
                </div>
            </div>
        </div>
    </div>

@endsection