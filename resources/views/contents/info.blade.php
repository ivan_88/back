<div class="row">
    <div class="col-md-4">@lang('interface.name')</div>
    <div class="col-md-8">{{ $content->firstname }} {{ $content->lastname }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.email')</div>
    <div class="col-md-8">{{ $content->email or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.phone')</div>
    <div class="col-md-8">{{ $content->phone or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.country')</div>
    <div class="col-md-8">{{ $content->country->full_name or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.city')</div>
    <div class="col-md-8">{{ $content->city or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.address')</div>
    <div class="col-md-8">{{ $content->address or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.zip')</div>
    <div class="col-md-8">{{ $content->zip or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.interest')</div>
    <div class="col-md-8">{{ $content->interest->$name or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.project')</div>
    <div class="col-md-8">{{ $content->project->$name or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.source')</div>
    <div class="col-md-8">{{ $content->source->$name or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.contact_by')</div>
    <div class="col-md-8">{{ $contacts[$content->contact_by][App::getLocale()] or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.user')</div>
    <div class="col-md-8">{{ $content->user->name or 'DELETED' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.active')/@lang('interface.archive')</div>
    <div class="col-md-8">{{ $content->active == 1 ? trans('interface.active') : trans('interface.archive') }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.rejection_reason')</div>
    <div class="col-md-8">{{ $content->rejection_reason or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.cause_of_recovery')</div>
    <div class="col-md-8">{{ $content->cause_of_recovery or 'None' }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.created')</div>
    <div class="col-md-8">{{ $content->created_at or 'None' }}</div>
</div>