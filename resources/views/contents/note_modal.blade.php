<div id="noteModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">@lang('interface.add_note')</h4>
            </div>
            <div class="modal-body">
                <div id="noteModalMsg"></div>
                <form id="addNoteForm">
                    <label class="control-label">@lang('interface.note')</label>
                    <textarea class="form-control" rows="5" id="note" name="note"></textarea>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" onclick="note.send()" class="btn btn-success">@lang('interface.add')</button>
            </div>
        </div>

    </div>
</div>