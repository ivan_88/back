@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="surf">
                        <h4 class="title">@lang('interface.edit') <b> {{$content->firstname . ' ' . $content->lastname}} </b></h4>
                        <p class="category"><a href="/" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/update/{{$content->id}}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="project_id" value=""/>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.firstname')</label>
                                        <input class="form-control" type="text" name="firstname"
                                               value="{{$content->firstname}}"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.lastname')</label>
                                        <input class="form-control" type="text" name="lastname"
                                               value="{{$content->lastname}}"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.phone')</label>
                                        <input class="form-control" type="text" name="phone"
                                               value="{{$content->phone}}"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.email')</label>
                                        <input class="form-control" type="text" name="email"
                                               value="{{$content->email}}"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.country')</label>
                                        <select name="country_id" class="form-control autoselect">
                                            @foreach($countries as $country)
                                                <option @if($content->country_id == $country->id)selected="selected"@endif value="{{$country->id}}">{{$country->full_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.interest')</label>
                                        <select name="interest_id" class="form-control">
                                            <option value="0"></option>
                                            @foreach($interests as $interest)
                                                <option @if($content->interest_id == $interest->id)selected="selected"@endif value="{{$interest->id}}">{{$interest->$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.source')</label>
                                        <select name="source_id" class="form-control">
                                            <option value="0"></option>
                                            @foreach($sources as $source)
                                                <option  @if($content->source_id == $source->id)selected="selected"@endif value="{{$source->id}}">{{$source->$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.contact_by')</label>
                                        <select name="contact_by" class="form-control">
                                            <option value="0"></option>
                                            @foreach($contacts as $key => $contact)
                                                <option @if($content->contact_by == $key)selected="selected"@endif
                                                value="{{ $key }}">
                                                    {{ $contact[App::getLocale()] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.city')</label>
                                        <input class="form-control" type="text" name="city" value="{{$content->city}}"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.address')</label>
                                        <input class="form-control" type="text" name="address"
                                               value="{{$content->address}}"/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.zip')</label>
                                        <input class="form-control" type="text" name="zip" value="{{$content->zip}}"/>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="form-group label-floating">
                                            <label class="control-label">@lang('interface.note')</label>
                                            <textarea class="form-control" name="notes"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="submit" value="@lang('interface.save')" class="btn btn-success pull-right"/>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection