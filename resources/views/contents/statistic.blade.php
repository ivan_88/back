@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header row "
                         @if($type == '2')data-background-color="orange"
                         @elseif($type == '4')data-background-color="red"
                         @elseif($type == '3')data-background-color="blue"
                         @elseif($type == '1')data-background-color="green"@endif >
                        <div class="col-md-6">
                            <h4 class="title">Statistics: <b></b></h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <ul>

                                <li class="statistic-buttons">
                                    <a href="/statistic?type=1" class="btn btn-xs btn-white btn-round material-icon_success">
                                        <i class="material-icons material-icon_success">assignment_returned</i>
                                        LEAD
                                    </a>
                                </li>
                                <li class="statistic-buttons">
                                    <a href="/statistic?type=2" class="btn btn-xs btn-white btn-round material-icon_success">
                                        <i class="material-icons material-icon_warning">assignment_returned</i>
                                        COM
                                    </a>
                                </li>
                                <li class="statistic-buttons">
                                    <a href="/statistic?type=3" class="btn btn-xs btn-white btn-round material-icon_success">
                                        <i class="material-icons material-icon_info">assignment_returned</i>
                                        NEG
                                    </a>
                                </li>
                                <li class="statistic-buttons">
                                    <a href="/statistic?type=4" class="btn btn-xs btn-white btn-round material-icon_success">
                                        <i class="material-icons material-icon_danger">assignment_returned</i>
                                        PRO
                                    </a>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <tr>
                            <th>User</th>
                            <th>Count customers</th>
                        </tr>
                        @if (count($statistics) > 0)
                            @foreach($statistics as $statistic)
                                <tr>
                                    <td>{{ $statistic->name or 'DELETED'}}</td>
                                    <td>{{ $statistic->count }}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>

@endsection