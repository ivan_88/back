<div id="archiveModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="archiveSendTitle" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <div id="archiveModalMsg"></div>
                <form id="archiveForm">
                    <label class="control-label">@lang('interface.reason')</label>
                    <input id="reason" name="reason" class="form-control" type="text"/>
                </form>

            </div>
            <div class="modal-footer">
                <button id="archiveSendButton" type="button" onclick="archive.send()" class="btn btn-success"></button>
            </div>
        </div>

    </div>
</div>