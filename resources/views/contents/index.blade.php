@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header row @if(!session('active')) archive__head @endif "
                         @if($type == 'COM')data-background-color="orange"
                         @elseif($type == 'PRO')data-background-color="red"
                         @elseif($type == 'NEG')data-background-color="blue"
                         @elseif($type == 'LEAD')data-background-color="green"
                         @elseif($type == 'Search')data-background-color="gray"@endif >
                        <div class="col-md-6">
                            <h3 class="title"><b>{{ $type }}</b>{{ $type != 'Search' ? 'S' : '' }}
                                : @if ($type != 'search') @if(!session('active')) @lang('interface.archive') @else @lang('interface.active') @endif @endif
                            </h3>
                            <p class="category">
                                @if ($type != 'Search')
                                    @if(session('active') == 1)
                                        <a href="?active=0" class="btn btn-sm btn-white static-color" rel="tooltip"
                                           title="To Archive">
                                            <i class="material-icons icon_normalized  material-icon_static">autorenew</i>
                                            <i class="material-icons icon_normalized  material-icon_static">save</i>
                                            @lang('interface.archive')
                                        </a>
                                    @else
                                        <a href="?active=1" class="btn btn-sm btn-white static-color" rel="tooltip"
                                           title="To Active">
                                            <i class="material-icons icon_normalized">autorenew</i>
                                            <i class="material-icons icon_normalized">flash_on</i>
                                            @lang('interface.active')
                                        </a>
                                    @endif
                                @endif
                            </p>

                        </div>

                        <div class="col-md-6 text-right">
                            <a href="/create?project={{ session('project') }}"
                               class="btn btn-xs btn-white btn-round material-icon_success">
                                <i class="material-icons">note_add</i>
                                @lang('interface.create')
                            </a>
                        </div>
                    </div>
                    <div class="card-content table-responsive">

                        <div class="card margin-xs">
                            <div class="col-xs-12">
                                <div class="col-lg-6">
                                    <form class="row form_no-padding" method="get" action="/">
                                        <div class="col-sm-2">
                                            <button class="btn btn-xs btn-white btn-round " rel="tooltip"
                                                    data-placement="bottom" title="@lang('interface.search')"
                                                    type="submit"
                                                    value="Search">
                                                <i class="material-icons">search</i>
                                            </button>
                                        </div>
                                        <div class="col-sm-5">
                                            <input class="form-control" type="text" name="search"
                                                   placeholder="@lang('interface.search')..."/>
                                        </div>
                                        <div class="col-sm-5">
                                            <select class="form-control" name="field">
                                                @foreach($searchFields as $key => $value)
                                                    <option value="{{ $key }}">@lang('interface.' . $value)</option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </form>
                                </div>
                                <div class="col-lg-6">
                                    <form class="row form_no-padding" method="get" action="/">
                                        <div class="col-sm-2">
                                            <button class="btn btn-xs btn-white btn-round "
                                                    rel="tooltip" data-placement="bottom"
                                                    title="@lang('interface.filter')" type="submit"
                                                    value="Filter">
                                                <i class="material-icons">sort</i>
                                            </button>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="interest_id">
                                                <option value="">@lang('interface.interest')</option>
                                                @foreach($interests as $interest)
                                                    <option @if(session('interest_id') == $interest->id)selected="selected"
                                                            @endif
                                                            value="{{ $interest->id }}">
                                                        {{ $interest->$name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <select class="form-control" name="source_id">
                                                <option value="">@lang('interface.source')</option>
                                                @foreach($sources as $source)
                                                    <option value="{{ $source->id }}"
                                                            @if(session('source_id') == $source->id)selected="selected"@endif >
                                                        {{ $source->$name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <a href="/?clear=1" class="btn btn-xs btn-white btn-round pull-right"
                                               rel="tooltip"
                                               data-placement="bottom" title="@lang('interface.clear_filters')">
                                                <i class="material-icons material-icon_danger">clear</i>
                                            </a>
                                        </div>

                                    </form>
                                </div>

                            </div>

                        </div>

                        <table class="table table-hover">
                            <thead>
                            <th>
                                <a rel="tooltip" title="@lang('interface.sort_by_id')"
                                   href="/?order_by=id&order_direction={{ session('order_direction') == 'asc' ? 'desc' : 'asc' }}">
                                    ID
                                    @if(session('order_by') && session('order_by')  == 'id')
                                        <i class="fa fa-sort-numeric-{{ session('order_direction') == 'asc' ? 'asc' : 'desc'}}"
                                           aria-hidden="true"></i>
                                    @endif
                                </a>
                            </th>
                            <th>
                                <a rel="tooltip" title="@lang('interface.sort_by_name')"
                                   href="/?order_by=lastname&order_direction={{ session('order_direction') == 'asc' ? 'desc' : 'asc' }}">
                                    @lang('interface.name')
                                    @if(session('order_by') && session('order_by')  == 'lastname')
                                        <i class="fa fa-sort-alpha-{{ session('order_direction') == 'asc' ? 'asc' : 'desc'}}"
                                           aria-hidden="true"></i>
                                </a>
                                @endif
                            </th>
                            <th>@lang('interface.contacts')</th>
                            <th>
                                <a rel="tooltip" title="@lang('interface.sort_by_sales_person')"
                                   href="/?order_by=user_id&order_direction={{ session('order_direction') == 'asc' ? 'desc' : 'asc' }}">
                                    @lang('interface.sales_person')
                                    @if(session('order_by') && session('order_by')  == 'user_id')
                                        <i class="fa fa-sort-alpha-{{ session('order_direction') == 'asc' ? 'asc' : 'desc'}}"
                                           aria-hidden="true"></i>
                                </a>
                                @endif
                            </th>
                            <th>
                                <a rel="tooltip" title="@lang('interface.sort_by_interest')"
                                   href="/?order_by=interest_id&order_direction={{ session('order_direction') == 'asc' ? 'desc' : 'asc' }}">
                                    @lang('interface.interest')
                                </a>
                                @if(session('order_by') && session('order_by')  == 'interest_id')
                                    <i class="fa fa-sort-alpha-{{ session('order_direction') == 'asc' ? 'asc' : 'desc'}}"
                                       aria-hidden="true"></i>
                                @endif
                            </th>
                            <th>
                                <a rel="tooltip" title="@lang('interface.sort_by_source')"
                                   href="/?order_by=source_id&order_direction={{ session('order_direction') == 'asc' ? 'desc' : 'asc' }}">
                                    @lang('interface.source')
                                    @if(session('order_by') && session('order_by')  == 'source_id')
                                        <i class="fa fa-sort-alpha-{{ session('order_direction') == 'asc' ? 'asc' : 'desc'}}"
                                           aria-hidden="true"></i>
                                </a>
                                @endif
                            </th>
                            <th>
                                <a rel="tooltip" title="@lang('interface.sort_by_contact_by')"
                                   href="/?order_by=contact_by&order_direction={{ session('order_direction') == 'asc' ? 'desc' : 'asc' }}">
                                    @lang('interface.contact_by')
                                    @if(session('order_by') && session('order_by')  == 'contact_by')
                                        <i class="fa fa-sort-alpha-{{ session('order_direction') == 'asc' ? 'asc' : 'desc'}}"
                                           aria-hidden="true"></i>
                                </a>
                                @endif
                            </th>
                            @if(session('active') == 0)
                                <th>@lang('interface.reason')</th>
                            @endif
                            <th class="th-actions">@lang('interface.actions')</th>
                            </thead>
                            <tbody>
                            @foreach($contents as $item)
                                <tr>
                                    <td> {{$item->id}}</td>
                                    <td>
                                        @if ($type == 'Search')
                                            @if ($item->type == 1)
                                                <a rel="tooltip" title="LEAD"
                                                   class="btn btn-simple btn-xs btn_square-icon">
                                                    <i class="material-icons material-icon_success">assignment_returned</i>
                                                </a>
                                            @elseif ($item->type == 2)
                                                <a rel="tooltip" title="COM"
                                                   class="btn btn-simple btn-xs btn_square-icon">
                                                    <i class="material-icons material-icon_warning">assignment_returned</i>
                                                </a>
                                            @elseif ($item->type == 3)
                                                <a rel="tooltip" title="NEG"
                                                   class="btn btn-simple btn-xs btn_square-icon">
                                                    <i class="material-icons material-icon_info">assignment_returned</i>
                                                </a>
                                            @elseif ($item->type == 4)
                                                <a rel="tooltip" title="COM"
                                                   class="btn btn-simple btn-xs btn_square-icon">
                                                    <i class="material-icons material-icon_danger">assignment_returned</i>
                                                </a>
                                            @endif

                                            @if ($item->active == 0)
                                                <a rel="tooltip" title="@lang('interface.archive')"
                                                   class="btn btn-simple btn-xs btn_square-icon">
                                                    <i class="material-icons material-icon_static">save</i>
                                                </a>
                                            @else
                                                <a rel="tooltip" title="@lang('interface.active')"
                                                   class="btn btn-simple btn-xs btn_square-icon">
                                                    <i class="material-icons ">flash_on</i>
                                                </a>
                                            @endif
                                        @endif

                                        {{$item->firstname}} {{$item->lastname}}
                                    </td>
                                    <td>
                                        {{$item->phone}}
                                    </td>
                                    <td>
                                        {{$item->user->name or 'DELETED'}}
                                    </td>
                                    <td>{{$item->interest->$name or ''}}</td>
                                    <td>{{$item->source->$name or ''}}</td>
                                    <td>{{array_key_exists($item->contact_by, $contacts) ? $contacts[$item->contact_by][App::getLocale()] : ''}}</td>
                                    @if(session('active') == 0)
                                        <td>
                                            {{$item->rejection_reason}}
                                        </td>
                                    @endif
                                    <td class="td-actions">
                                        <a href="/edit/{{$item->id}}" type="button" rel="tooltip"
                                           title="@lang('interface.edit')"
                                           class="btn btn-simple btn-xs">
                                            <i class="material-icons">edit</i>
                                        </a>

                                        @if($item->active == 1 && $movingActions[$item->type]['back'] && \App\Helpers\ContentHelper::canUserMove($movingActions[$item->type]['back']))
                                            <a href="/move/{{$item->id}}?action={{ $movingActions[$item->type]['back'] }}"
                                               type="button"
                                               rel="tooltip"
                                               title="@lang('interface.return_to') {{ $types[$item->type - 1] }} "
                                               class="btn btn-simple btn-xs  color_{{$types[$item->type - 1] }}">
                                                <i class="material-icons">arrow_back</i></a>
                                        @endif

                                        @if($item->active == 1 && $movingActions[$item->type]['forward'] && \App\Helpers\ContentHelper::canUserMove($movingActions[$item->type]['forward']))
                                            <a href="/move/{{$item->id}}?action={{ $movingActions[$item->type]['forward'] }}"
                                               type="button"
                                               rel="tooltip"
                                               title="@lang('interface.move_to') {{ $types[$item->type + 1] }} "
                                               class="btn btn-simple btn-xs color_{{$types[$item->type + 1] }}">
                                                <i class="material-icons">arrow_forward</i></a>
                                        @endif

                                        @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager'))

                                            @if($item->active == 1)
                                                <button type="button" rel="tooltip"
                                                        title="@lang('interface.to_archive')"
                                                        class="btn  btn-simple btn-xs archive"
                                                        onclick="archive.modal({{ $item->id }}, 0, '@lang('interface.to_archive')')">
                                                    <i class="material-icons">archive</i>
                                                </button>
                                            @else
                                                <button type="button" rel="tooltip"
                                                        title="@lang('interface.to_active')"
                                                        class="btn  btn-simple btn-xs archive"
                                                        onclick="archive.modal({{ $item->id }}, 1, '@lang('interface.to_active')')">
                                                    <i class="material-icons">archive</i>
                                                </button>
                                            @endif
                                            <a href="/log/{{$item->id}}" type="button" rel="tooltip"
                                               title="@lang('interface.log')"
                                               class="btn  btn-simple btn-xs">
                                                <i class="material-icons">history</i>
                                            </a>

                                        @endif
                                        <a href="/notes/{{$item->id}}" type="button" rel="tooltip"
                                           title="@lang('interface.notes')"
                                           class="btn  btn-simple btn-xs">
                                            <i class="material-icons">notes</i>
                                        </a>
                                        <button type="button"
                                                rel="tooltip"
                                                title="@lang('interface.add_note')"
                                                class="btn  btn-simple btn-xs"
                                                onclick="note.modal({{$item->id}})">
                                            <i class="material-icons">note</i>
                                        </button>
                                        <button type="button"
                                                rel="tooltip"
                                                title="@lang('interface.info')"
                                                class="btn  btn-simple btn-xs"
                                                onclick="info.modal({{$item->id}})">
                                            <i class="material-icons">info</i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $contents->links('parts.pagination') }}
                </div>
            </div>
        </div>
    </div>

    @include('contents.archive_modal')
    @include('contents.note_modal')
    @include('contents.info_modal')

    <script src="{{ asset('js/contents.js') }}"></script>

@endsection