@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="green">
                        <h4 class="title">@lang('interface.create') LEAD</h4>
                        <p class="category"><a href="/" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/store" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="project_id" value="{{$project}}"/>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.firstname')</label>
                                        <input class="form-control"
                                               value="{{ session('data.firstname') }}"
                                               type="text"
                                               name="firstname" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.lastname')</label>
                                        <input class="form-control"
                                               type="text"
                                               name="lastname"
                                               value="{{ session('data.lastname') }}" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.phone')</label>
                                        <input class="form-control"
                                               type="text"
                                               name="phone"
                                               value="{{ session('data.phone') }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.email')</label>
                                        <input class="form-control"
                                               type="text"
                                               name="email"
                                               value="{{ session('data.email') }}" />
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.country')</label>
                                        <select name="country_id" class="form-control autoselect">
                                            @foreach($countries as $country)
                                                    <option @if((session('data.country_id') == $country->id) || (!session('data.country_id') && $country->id == 804))selected="selected" @endif value="{{$country->id}}">{{$country->full_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.interest')</label>
                                        <select name="interest_id" class="form-control">
                                            <option value="0"></option>
                                            @foreach($interests as $interest)
                                                <option @if(session('data.interest_id') == $interest->id)selected="selected"@endif value="{{$interest->id}}">{{$interest->$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.source')</label>
                                        <select name="source_id" class="form-control">
                                            <option value="0"></option>
                                            @foreach($sources as $source)
                                                <option  @if(session('data.source_id') == $source->id)selected="selected"@endif value="{{$source->id}}">{{$source->$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">@lang('interface.contact_by')</label>
                                        <select name="contact_by" class="form-control">
                                            <option value="0"></option>
                                            @foreach($contacts as $key => $contact)
                                                <option @if(session('data.contact_by') == $key)selected="selected"@endif
                                                value="{{ $key }}">
                                                    {{ $contact[App::getLocale()] }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.city')</label>
                                        <input class="form-control"
                                               type="text"
                                               name="city"
                                               value="{{ session('data.city') }}" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.address')</label>
                                        <input class="form-control"
                                               type="text"
                                               name="address"
                                               value="{{ session('data.address') }}" />
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.zip')</label>
                                        <input class="form-control"
                                               type="text"
                                               name="zip"
                                               value="{{ session('data.zip') }}" />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="form-group label-floating">
                                                <label class="control-label">@lang('interface.note')</label>
                                                <textarea class="form-control" rows="3" name="notes">
                                                    {{ session('data.notes') }}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="@lang('interface.create')" class="btn btn-success pull-right"/>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection