@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header row" data-background-color="blue">

                        <div class="col-md-6">
                            <h4 class="title">@lang('interface.projects')</h4>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="/projects/create" class="btn btn-xs btn-white btn-round material-icon_success"><i
                                        class="material-icons">person_add</i>@lang('interface.create')</a>
                        </div>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center"><b>Id</b></th>
                                <th class="text-center"><b>@lang('interface.name_en')</b></th>
                                <th class="text-center"><b>@lang('interface.name_uk')</b></th>
                                <th class="text-center"><b>@lang('interface.created')</b></th>
                                <th class="text-center"><b>@lang('interface.updated')</b></th>
                                <th class="th-actions"><b>@lang('interface.actions')</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td class="text-center">{{ $project->id }}</td>
                                    <td class="text-center">{{ $project->name_en }}</td>
                                    <td class="text-center">{{ $project->name_uk }}</td>
                                    <td class="text-center">{{ $project->created_at }}</td>
                                    <td class="text-center">{{ $project->updated_at }}</td>
                                    <td class="td-actions">
                                        <a href="/projects/edit/{{$project->id}}" rel="tooltip" title="@lang('interface.edit')"
                                           class="btn btn-simple btn-xs">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a href="/projects/delete/{{$project->id}}" rel="tooltip" title="@lang('interface.delete')"
                                           class="btn btn-simple btn-xs">
                                            <i class="material-icons">delete</i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection