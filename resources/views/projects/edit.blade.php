@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="surf">
                        <h4 class="title">@lang('interface.edit') {{ $project->name }}</h4>
                        <p class="category"><a href="/projects" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/projects/update/{{ $project->id }}" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                                <div class="form-group label-floating col-md-3">
                                    <label class="control-label">@lang('interface.name_en')</label>
                                    <input class="form-control" type="text" name="name_en" value="{{ $project->name_en }}" />
                                </div>

                                <div class="form-group label-floating col-md-3">
                                    <label class="control-label">@lang('interface.name_uk')</label>
                                    <input class="form-control" type="text" name="name_uk" value="{{ $project->name_uk }}" />
                                </div>

                                <input type="submit" value="@lang('interface.save')" class=" submit-btn btn btn-success pull-right"/>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection