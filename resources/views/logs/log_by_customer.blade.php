@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-plain">
                <div class="card-header row" data-background-color="surf">
                    <div class="col-xs-8">
                        <h4 class="title">@lang('interface.log') : <b>{{$content->firstname . ' ' . $content->lastname}}</b></h4>
                        <p class="category"><a href="/" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>
                    <div class="col-xs-4">
                        <a href="/log/{{ $content->id }}?excel=1" class="btn btn-xs btn-white btn-round material-icon_success pull-right">
                           <i class="fa fa-file-excel-o" aria-hidden="true"></i> @lang('interface.get_excel')
                        </a>
                    </div>
                </div>
                <div class="card-content table-responsive">
                    @include('logs.log_by_customer_table')
                 </div>
                {{ $log->links('parts.pagination') }}
            </div>
        </div>
    </div>
</div>

@endsection