@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header row" data-background-color="surf">
                        <h4 class="title col-xs-10">@lang('interface.global_log')<b></b></h4>
                        <div class="col-xs-2">
                            <a href="/log?excel=1" class="btn btn-xs btn-white btn-round material-icon_success pull-right">
                                <i class="fa fa-file-excel-o" aria-hidden="true"></i> @lang('interface.get_excel')
                            </a>
                        </div>
                    </div>
                    <div class="card-content table-responsive">
                        <div class="card margin-xs">
                            <form class="col-sm-12 form_no-padding"  action="/log" method="get">
                                <div class="form-group col-sm-4">
                                    <div class='input-group date' data-provide="datepicker" id='start'>
                                        <span class="input-group-addon " ><i class="material-icons material-icon_neutral">date_range</i></span>
                                        <input type='text' class="form-control" placeholder="@lang('interface.from'):" name="start"/>
                                    </div>
                                </div>
                                <div class="form-group col-sm-4">
                                    <div class='input-group date' data-provide="datepicker"  id='end'>
                                        <span class="input-group-addon" ><i class="material-icons material-icon_neutral">date_range</i></span> 
                                        <input type='text' class="form-control" placeholder="@lang('interface.to'):"  name="end"/>
                                    </div>
                                </div>
                                <div class="form-group col-sm-4">
                                    <button class="btn btn-xs btn-white btn-round" rel="tooltip" data-placement="bottom" title="@lang('interface.apply_filters')"   type="submit" value="filter" />
                                        <i class="material-icons material-icon_info">sort</i><span>@lang('interface.filter')</span>
                                    </button>
                                    <a href="/log?clear=1" class="btn btn-xs btn-white btn-round pull-right"  rel="tooltip" data-placement="bottom" title="@lang('interface.clear_filters')">
                                        <i class="material-icons material-icon_danger">clear</i>@lang('interface.clear_filters')
                                    </a>
                                </div>
                        </form>
                        </div>

                        @include('logs.global_log_table')
                    </div>
                    {{ $log->links('parts.pagination') }}
                </div>
            </div>
        </div>
    </div>

@endsection