<table class="table table-hover">
    <thead>
    <tr>
        <th class="text-center"><b>@lang('interface.customer') </b></th>
        <th class="text-center"><b>@lang('interface.action') </b></th>
        <th class="text-center"><b>@lang('interface.date') </b></th>
    </tr>
    </thead>
    <tbody>
    @foreach($log as $row)
        <tr>
            <td class="text-center">
                {{ isset($row->customer) ? $row->customer->firstname. ' ' . $row->customer->lastname : 'DELETED' }}
            </td>
            <td class="text-center">
                @if ($row->action == \App\ContentLog::ACTION_ARCHIVE && isset($row->content))
                    {{ trans('log.' . $actions[$row->action]) }}: "{{ $row->content->rejection_reason }}"
                @elseif ($row->action == \App\ContentLog::ACTION_ACTIVE && isset($row->content))
                    {{ trans('log.' . $actions[$row->action]) }}: "{{ $row->content->cause_of_recovery }}"
                @else
                    {{ trans('log.' . $actions[$row->action]) }}
                @endif
            </td>
            <td class="text-center">{{Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $row->created_at)->format('d-m-Y H:i')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>