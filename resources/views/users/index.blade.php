@extends('layouts.app')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-plain">
                <div class="card-header row" data-background-color="surf">
                                
                    <div class="col-md-6">
                        <h4 class="title">@lang('interface.users')</h4>
                    </div>             
                    <div class="col-md-6 text-right">
                        <a href="/users/create" class="btn btn-xs btn-white btn-round material-icon_success"><i class="material-icons">person_add</i>@lang('interface.create')</a>
                    </div>                    
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th class="text-center"><b>@lang('interface.name')</b></th>
                                <th class="text-center"><b>@lang('interface.email')</b></th>
                                <th class="text-center"><b>@lang('interface.role')</b></th>
                                <th class="text-center"><b>@lang('interface.actions')</b></th>
                            </tr>
                        </thead>
                        <tbody> 
                            @foreach($users as $user)
                    <tr>
                        <td class="text-center">{{$user->name}}</td>
                        <td class="text-center">{{$user->email}}</td>
                        <td class="text-center">
                            @foreach($user->roles as $role)
                                {{$role->name}}
                            @endforeach
                        </td>

                        <td class="text-center ">
                            @if ($authUser->hasRole('admin') || $user->hasRole('broker'))
                                <a href="/users/edit/{{$user->id}}" rel="tooltip" title="@lang('interface.edit')"
                                   class="btn btn-simple btn-xs">
                                    <i class="material-icons">edit</i>
                                </a>
                                <a href="/users/delete/{{$user->id}}" rel="tooltip" title="@lang('interface.delete')"
                                   class="btn btn-simple btn-xs">
                                    <i class="material-icons">delete</i>
                                </a>
                                <a href="/users/log/{{$user->id}}" rel="tooltip" title="@lang('interface.log')"
                                   class="btn btn-simple btn-xs">
                                    <i class="material-icons">history</i>
                                </a>
                            @endif
                        </td>
                    </tr>
                    @endforeach                     
                        </tbody>
                    </table>
                 </div>
                {{ $users->links('parts.pagination') }}
            </div>
        </div>
    </div>
</div>

@endsection