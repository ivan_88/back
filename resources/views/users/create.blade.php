@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="surf">
                        <h4 class="title">@lang('interface.create_user')</h4>
                        <p class="category"><a href="/users" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/users/store" method="post">
                            {{ csrf_field() }}
                            <div class="row">
                            <div class="form-group label-floating col-md-3">
                                <label class="control-label">@lang('interface.name')</label>
                                <input class="form-control" type="text" name="name"/>
                            </div>
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.email')</label>
                                        <input class="form-control" type="text" name="email" required/>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.password')</label>
                                        <input id="pass" class="form-control password" type="password" name="password"
                                               required/>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div id="passConfirmGroup" class="form-group label-floating label-floating">
                                        <label class="control-label ">@lang('interface.password_confirmation')</label>
                                        <input id="passConfirm" class="form-control password" type="password"
                                               name="password_confirmation" value="" required/>
                                        <span class="material-icons form-control-feedback">clear</span>
                                    </div>
                                </div>

                                @role('admin')
                                <div class="col-md-3">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.role')</label>
                                        <select name="role_id" class="form-control">
                                            @foreach($roles as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @endrole

                                <input type="submit" value="@lang('interface.create')" class=" submit-btn btn btn-success pull-right"/>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection