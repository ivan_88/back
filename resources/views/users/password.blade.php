@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header row" data-background-color="surf">
                        <h4 class="title">@lang('interface.change_password')</h4>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if (session('success'))
                            <div class="alert alert-success">{{ session('success') }}</div>
                        @endif

                        <form class="card-content" action="/users/password" method="post">
                            {{ csrf_field() }}

                            <div class="form-group label-floating col-md-4">
                                <label class="control-label">@lang('interface.old_password')</label>
                                <input class="form-control" type="password" name="old_password"/>
                            </div>

                            <div class="form-group label-floating col-md-4">
                                <label class="control-label">@lang('interface.new_password')</label>
                                <input class="form-control" type="password" name="password"/>
                            </div>

                            <div class="form-group label-floating col-md-4">
                                <label class="control-label">@lang('interface.new_password_confirmation')</label>
                                <input class="form-control" type="password" name="password_confirmation"/>
                            </div>

                            <input class="btn btn-success pull-right" type="submit" value="@lang('interface.save')"/>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection