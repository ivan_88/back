@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header row" data-background-color="blue">
                        <h4 class="title">@lang('interface.edit'): <b>#{{$user->id}} | {{$user->name}}</b></h4>
                        <p class="category"><a href="/users" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/users/update/{{$user->id}}" method="post">
                            {{ csrf_field() }}

                            <div class="form-group label-floating col-md-6">
                                <label class="control-label">@lang('interface.name')</label>
                                <input class="form-control" type="text" name="name" value="{{$user->name}}"/>
                            </div>

                            <div class="form-group label-floating col-md-6">
                                <label class="control-label">@lang('interface.email')</label>
                                <input class="form-control" type="text" name="email" value="{{$user->email}}"/>
                            </div>

                            @if (Auth::user()->hasRole('admin') && !$user->hasRole('admin'))
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label class="control-label">@lang('interface.role')</label>
                                    <select name="role_id" class="form-control">
                                        @foreach($roles as $role)
                                            <option @if($user->hasRole($role->name))selected="selected"@endif value="{{$role->id}}">{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif

                            <input class="btn btn-success pull-right" type="submit" value="@lang('interface.save')"/>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection