<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CRM') }}</title>
    
    <!-- CSS Libraries -->
    <link href="{{ asset('css/css-libs.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet"/>

    <!-- Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="../../img/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="../../img/favicon-16x16.png">
    <link rel="manifest" href="../../img/manifest.json">
    <link rel="mask-icon" href="../../img/safari-pinned-tab.svg" color="#4d4d4d">
    <link rel="shortcut icon" href="../../img/favicon.ico">
    <meta name="msapplication-config" content="../../img/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">

</head>
<body>
<div class="wrapper">
    @if(Auth::check())
        <div class="sidebar" data-color="red" data-image="http://belgianvillage.com/images/pustomyty.jpg">


            <div class="logo">
                <a class="simple-text" href="{{ url('/') }}">
                    <img src="../../img/bv-crm-logo.svg" alt="CRM">
                </a>
                <h3 class="text-center">
                    {{ session('project_name') }}
                </h3>
            </div>

            <div class="sidebar-wrapper">

                <ul class="nav">

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle " data-toggle="dropdown">
                            <i class="material-icons">star</i>
                            <p>@lang('interface.projects')</p>
                        </a>
                        <ul class="dropdown-menu">
                            @foreach($projects as $project)
                                <li><a href="/?project={{$project->id}}"><p>{{$project->$name}}</p></a></li>
                            @endforeach
                        </ul>
                    </li>
                    @if (Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager'))
                        <li>

                            <a href="/users">
                                <i class="material-icons">account_circle</i>
                                <p>@lang('interface.users')</p>
                            </a>

                        </li>
                        <li>
                            <a href="/log">
                                <i class="material-icons">history</i>
                                <p>@lang('interface.log')</p>
                            </a>
                        </li>

                    @endif

                    <li>
                        <a href="/reminders?today=1">
                            <i class="material-icons">alarm</i>
                            <p>@lang('interface.reminders') <span id="remindersCount">()</span></p>
                        </a>
                    </li>

                    <li>
                        <a href="/?type=1">
                            <i class="material-icons material-icon_success">assignment_returned</i>
                            <p>LEAD</p>
                        </a>
                    </li>
                    <li>
                        <a href="/?type=2">
                            <i class="material-icons material-icon_warning">assignment_returned</i>
                            <p>COM</p>
                        </a>
                    </li>
                    <li>
                        <a href="/?type=3">
                            <i class="material-icons material-icon_info">assignment_returned</i>
                            <p>NEG</p>
                        </a>
                    </li>
                    <li>
                        <a href="/?type=4">
                            <i class="material-icons material-icon_danger">assignment_returned</i>
                            <p>PRO</p>
                        </a>
                    </li>

                </ul>

            </div>
        </div>
    @endif
    <div class="main-panel  @if(!Auth::check()) main-panel_login @endif ">

        <nav class="navbar navbar-transparent navbar-absolute">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>
                @if($_SERVER['REQUEST_URI'] != '/login')
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle "
                                   data-toggle="dropdown">
                                    <span>@lang('interface.language')</span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="/locale/en">EN</a></li>
                                    <li><a href="/locale/uk">UK</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle "
                                   data-toggle="dropdown">
                                    <span>@if (Auth::check()){{ Auth::user()->name }}@endif</span>
                                    <i class="material-icons">person</i>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="/users/self">@lang('interface.edit_account')</a></li>
                                    <li><a href="/users/password">@lang('interface.change_password')</a></li>
                                    @role('admin')
                                    <li><a href="/projects">@lang('interface.projects')</a></li>
                                    <li><a href="/interests">@lang('interface.interests')</a></li>
                                    <li><a href="/statistic">@lang('interface.statistics')</a></li>
                                    @endrole
                                    <li><a href="../logout">@lang('interface.logout')</a></li>
                                </ul>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="material-icons" >alarm</i>
                                    <span id="upRemindersCounterNotify">
                                        <span id="upRemindersCounter"></span>
                                    </span>
                                    <p class="hidden-lg hidden-md">Notifications</p>
                                <div class="ripple-container"></div></a>

                                <ul class="dropdown-menu" id="listReminders">

                                </ul>
                            </li>
                        </ul>
                    </div>
                @endif
            </div>
        </nav>

        <div class="content">
            @yield('content')
        </div>

    </div>
</div>

@include('reminders.reminder_modal')

<!--   Core JS Files   -->

<script src="{{ asset('js/js-libs.js') }}" type="text/javascript"></script>


<!-- Custom Scripts -->
<script src="{{ asset('js/main.js') }}"></script>

</body>
</html>
