@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="surf">
                        <h4 class="title">@lang('interface.create_reminder')</h4>
                        <p class="category"><a href="/reminders" class="btn btn-sm btn-white static-color" >@lang('interface.back')</a></p>
                    </div>

                    <div class="card-content">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form class="card-content" action="/reminders/store" method="post">
                            {{ csrf_field() }}
                            <div class="row">

                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.customer')</label>
                                        <select name="content_id" class="form-control">
                                            @foreach($contents as $content)
                                                <option value="{{ $content->id }}">{{ $content->firstname }} {{ $content->lastname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group label-floating">
                                        <label class="control-label">@lang('interface.assigned_to')</label>
                                        <div class='input-group date'>                   
                                            <input type='text' class="form-control"  name="assigned_to"  id="assigned_to" />
                                            <span class="input-group-addon"><i class="material-icons material-icon_neutral">date_range</i></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group label-floating ">
                                        <label class="control-label">@lang('interface.note')</label>
                                        <textarea class="form-control" rows="3" id="note" name="note"></textarea>
                                    </div>
                                </div>

                                <input type="submit" value="@lang('interface.create')" class=" submit-btn btn btn-success pull-right"/>
                                <div class="clearfix"></div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection