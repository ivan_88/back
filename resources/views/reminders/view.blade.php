<div class="row">
    <div class="col-md-4">@lang('interface.customer')</div>
    <div class="col-md-8">{{ $reminder->content->firstname }} {{ $reminder->content->lastname }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.note')</div>
    <div class="col-md-8">{{ $reminder->note }}</div>
</div>

<div class="row">
    <div class="col-md-4">@lang('interface.assigned_to')</div>
    <div class="col-md-8">{{ $reminder->assigned_to }}</div>
</div>