@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-plain">
                    <div class="card-header row" data-background-color="surf">

                        <div class="col-md-6">
                            <h4 class="title">@lang('interface.reminders'): <b>{{ session('executed') == 1 ? trans('interface.executed') : trans('interface.active') }}</b> | <b>{{ session('today') == 1 ? trans('interface.today') : trans('interface.all_times') }}</b></h4>

                            @if (session('executed') == 0)
                            <a href="/reminders?done=1" class="btn btn-xs btn-white btn-round color_neutral">
                                  <i class="material-icons">done</i> @lang('interface.executed')</a>
                            @else
                                <a href="/reminders?done=0" class="btn btn-xs btn-white btn-round material-icon_warning">
                                     <i class="material-icons">flash_on</i> @lang('interface.active')</a>
                            @endif

                            @if (session('today') == 0)
                                <a href="/reminders?today=1" class="btn btn-xs btn-white btn-round color_neutral">
                                    <i class="material-icons">today</i> @lang('interface.today')</a>
                            @else
                                <a href="/reminders?today=0" class="btn btn-xs btn-white btn-round color_neutral">
                                    <i class="material-icons">date_range</i> @lang('interface.all_times')</a>
                            @endif
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="/reminders/create" class="btn btn-xs btn-white btn-round material-icon_success">
                                <i class="material-icons">alarm</i>
                                @lang('interface.create')</a>

                        </div>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="text-center"><b>Id</b></th>
                                <th class="text-center"><b>@lang('interface.for_customer')</b></th>
                                <th class="text-center"><b>@lang('interface.note')</b></th>
                                <th class="text-center"><b>@lang('interface.executed')</b></th>
                                <th class="text-center"><b>@lang('interface.assigned_to')</b></th>
                                <th class="text-center"><b>@lang('interface.actions')</b></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reminders as $reminder)
                                <tr>
                                    <td class="text-center">{{ $reminder->id }}</td>
                                    <td class="text-center">{{ $reminder->content->firstname }} {{ $reminder->content->lastname }}</td>
                                    <td class="text-center">{{ $reminder->note or 'DELETED' }}</td>
                                    <td class="text-center">{{ $reminder->done == 1 ? 'Yes' : 'No' }}</td>
                                    <td class="text-center">{{ $reminder->assigned_to }}</td>
                                    <td class="text-center">
                                        <a href="/reminders/edit/{{$reminder->id}}" rel="tooltip" title="@lang('interface.edit')"
                                           class="btn btn-simple btn-xs">
                                            <i class="material-icons">edit</i>
                                        </a>
                                        <a onclick="reminder.done({{$reminder->id}}, {{ session('executed') == 1 ? 0 : 1 }})"
                                           rel="tooltip"
                                           title="{{ session('executed') == 0 ? trans('interface.execute') : trans('interface.return') }}"
                                           class="btn btn-simple btn-xs">
                                            <i class="material-icons">{{ session('executed') == 0 ? 'check' : 'cancel' }}</i>
                                        </a>
                                        <a href="/reminders/delete/{{$reminder->id}}" rel="tooltip" title="@lang('interface.delete')"
                                           class="btn btn-simple btn-xs">
                                            <i class="material-icons">delete</i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $reminders->links() }}
                </div>
            </div>
        </div>
    </div>

@endsection