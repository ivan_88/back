let archive = {
    id: null,
    action: null,
    modal: function (id, action, text) {
        $('#archiveModal').modal();
        this.id = id;
        this.action = action;
        $('#archiveSendButton').html(text);
        $('#archiveSendTitle').html(text);
    },
    send: function () {
        let data = { 'reason': $('#reason').val(), 'action': this.action };
        data = JSON.stringify(data);
        $.ajax({
            url: '/archive/' + this.id,
            data: data,
            method: 'POST',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
                'Content-type': 'application/json'
            },
            success: function () {
                window.location.reload();
            },
            error: function (errors) {
                let messages = JSON.parse(errors.responseText);
                let messagesHTML = '<div class="alert alert-danger"><ul>';
                for (let i in messages.errors) {
                    for (let j in messages.errors[i]) {
                        messagesHTML += `<li>${messages.errors[i][j]}</li>`;
                    }
                }
                messagesHTML += '</ul></div>';
                $('#modalMsg').html(messagesHTML);
            }
        });
    }
};

let note = {
    id: null,
    modal: function (id) {
        $('#noteModal').modal();
        this.id = id;
    },
    send: function () {
        let data = {'note': $('#note').val()};
        data = JSON.stringify(data);
        $.ajax({
            url: '/note/' + this.id,
            data: data,
            method: 'POST',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
                'Content-type': 'application/json'
            },
            success: function () {
                $('#noteModal').modal('hide');
            },
            error: function (errors) {
                let messages = JSON.parse(errors.responseText);
                let messagesHTML = '<div class="alert alert-danger"><ul>';
                for (let i in messages.errors) {
                    for (let j in messages.errors[i]) {
                        messagesHTML += `<li>${messages.errors[i][j]}</li>`;
                    }
                }
                messagesHTML += '</ul></div>';
                $('#noteModalMsg').html(messagesHTML);
            }
        })
    }
};

let info = {
    modal: function (id) {
        $('#infoModal').modal();
        $.ajax({
            url: '/info/' + id,
            method: 'get',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
                'Content-type': 'application/json'
            },
            success: function (data) {
                $('#info').html(data);
            },
        });
    }
};
//# sourceMappingURL=contents.js.map
