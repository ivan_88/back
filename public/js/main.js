let reminder = {
    id: null,
    modal: function (id) {
        this.id = id;
        $('#reminderModal').modal();
        $.ajax({
            url: '/reminders/show/' + id,
            method: 'get',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
                'Content-type': 'application/json'
            },
            success: function (data) {
                $('#reminderInfo').html(data);
            },
        });
    },
    get: function () {
        $.ajax({
            url: '/reminders/today',
            method: 'get',
            success: function (data) {
                data = JSON.parse(data);
                $('#remindersCount').html(`(${data.count})`);
                let list = '';
                if (data.count > 0 && data.list) {
                    $('#upRemindersCounter').html(data.count);
                    $('#upRemindersCounterNotify').addClass('notification');

                    for (let i in data.list) {
                        list += `<li><a>
                                <span class="pseudolink" onclick="reminder.modal(${data.list[i].id})">${data.list[i].note.substr(0, 25)}...</span>
                            </a></li>`
                    }
                } else {
                    list = '<li><span class="pseudolink" >None</span></li>';
                    $('#upRemindersCounter').html();
                    $('#upRemindersCounterNotify').removeClass('notification');
                }
                $('#listReminders').html(list);
            }
        })
    },
    done: function (id = null, done = 1) {
        id = id ? id : this.id;
        $.ajax({
            url: `/reminders/done/${id}?done=${done}`,
            success: function () {
                $('#reminderModal').modal('hide');
                window.location.reload();
            }
        });
    }
};

$(document).ready(function () {
    $("#assigned_to").datetimepicker();
    reminder.get();
    /* Select-2 Init */
    $('.autoselect').select2();
});

/* Passwords Confirmation Compare */

$( ".password" ).keyup(function() {
    if( $('#pass').val() != $('#passConfirm').val()  ) {
        $('#passConfirmGroup').addClass(' has-error');
    }
});
//# sourceMappingURL=main.js.map

//# sourceMappingURL=main.js.map
