<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => 'locale'], function() {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout');


    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.request');


    Route::group(['middleware' => 'auth'], function() {
        Route::get('/', 'ContentsController@index');
        Route::get('/create', 'ContentsController@create');
        Route::post('/store', 'ContentsController@store');
        Route::get('/edit/{id}', 'ContentsController@edit');
        Route::post('/update/{id}', 'ContentsController@update');
        Route::get('/notes/{id}', 'ContentsController@notes');
        Route::post('/note/{id}', 'ContentsController@addNote');
        Route::get('/info/{id}', 'ContentsController@show');
        Route::get('/move/{id}', 'ContentsController@move');

        Route::get('/users/self', 'UsersController@editSelf');
        Route::get('/users/password', 'UsersController@changePasswordForm');
        Route::post('/users/update/{id}', 'UsersController@update');
        Route::post('/users/password', 'UsersController@changePassword');

        Route::get('/reminders', 'RemindersController@index');
        Route::get('/reminders/create', 'RemindersController@create');
        Route::post('/reminders/store', 'RemindersController@store');
        Route::get('/reminders/edit/{id}', 'RemindersController@edit');
        Route::post('/reminders/update/{id}', 'RemindersController@update');
        Route::get('/reminders/delete/{id}', 'RemindersController@delete');
        Route::get('/reminders/today', 'RemindersController@getTodayRemindersCount');
        Route::get('/reminders/done/{id}', 'RemindersController@done');
        Route::get('/reminders/show/{id}', 'RemindersController@show');



        Route::group(['middleware' => 'role:admin|manager'], function() {
            Route::post('/archive/{id}', 'ContentsController@archiveOrActive');
            Route::get('/log/{id}', 'LogsController@viewLogByCustomer');


            Route::get('/users', 'UsersController@index');
            Route::get('/users/create', 'UsersController@create');
            Route::post('/users/store', 'UsersController@store');
            Route::get('/users/edit/{id}', 'UsersController@edit');

            Route::get('/users/delete/{id}', 'UsersController@delete');
            Route::get('/users/log/{id}', 'LogsController@viewLogByUser');

            Route::get('/log', 'LogsController@viewGlobalLog');
        });

        Route::group(['middleware' => 'role:admin'], function() {
            Route::get('/projects', 'ProjectsController@index');
            Route::get('/projects/create', 'ProjectsController@create');
            Route::post('/projects/store', 'ProjectsController@store');
            Route::get('/projects/edit/{id}', 'ProjectsController@edit');
            Route::post('/projects/update/{id}', 'ProjectsController@update');
            Route::get('/projects/delete/{id}', 'ProjectsController@delete');

            Route::get('/interests', 'InterestsController@index');
            Route::get('/interests/create', 'InterestsController@create');
            Route::post('/interests/store', 'InterestsController@store');
            Route::get('/interests/edit/{id}', 'InterestsController@edit');
            Route::post('/interests/update/{id}', 'InterestsController@update');
            Route::get('/interests/delete/{id}', 'InterestsController@delete');

            Route::get('/statistic', 'StatisticController@statistic');
        });
    });

    Route::get('/404', function() {
        return view('404');
    });
    Route::get('/403', function() {
        return view('403');
    });
});

Route::get('/locale/{slug}', function ($slug) {
    App::setLocale($slug);
    \Illuminate\Support\Facades\Session::put('locale', $slug);
    \Illuminate\Support\Facades\Session::forget('project_name');
    return redirect('/');
});