<?php

use Illuminate\Http\Request;
use Laravel\Passport\Passport;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/init', 'Api\InitController@init');
    
    Route::get('/customers', 'Api\ContentsController@index');
    Route::get('/customers/show/{id}', 'Api\ContentsController@show');
    Route::post('/customers/store', 'Api\ContentsController@store');
    Route::post('/customers/update/{id}', 'Api\ContentsController@update');
    Route::get('/customers/move/{id}', 'Api\ContentsController@move');
    Route::post('/customers/active/{id}', 'Api\ContentsController@archiveOrActive');
    Route::get('/customers/notes/{id}', 'Api\ContentsController@notes');
    Route::post('/customers/note/{id}', 'Api\ContentsController@addNote');

    Route::group(['middleware' => 'role:admin|manager'], function() {
        Route::get('/users', 'Api\UsersController@index');
        Route::post('/users/store', 'Api\UsersController@store');
        Route::post('/users/update/{id}', 'Api\UsersController@update');
        Route::get('/users/delete/{id}', 'Api\UsersController@delete');
    });

    Route::group(['middleware' => 'role:admin|manager|partner'], function() {
        Route::get('/users/show/{id}', 'Api\UsersController@show');
    });

    Route::post('/users/password', 'Api\UsersController@changePassword'); // yet no working

    Route::group(['middleware' => 'role:admin'], function() {
        Route::get('/projects', 'Api\ProjectsController@index');
        Route::post('/projects/store', 'Api\ProjectsController@store');
        Route::get('/projects/show/{id}', 'Api\ProjectsController@show');
        Route::post('/projects/update/{id}', 'Api\ProjectsController@update');
        Route::get('/projects/delete/{id}', 'Api\ProjectsController@delete');

        Route::get('/interests', 'Api\InterestsController@index');
        Route::post('/interests/store', 'Api\InterestsController@store');
        Route::get('/interests/show/{id}', 'Api\InterestsController@show');
        Route::post('/interests/update/{id}', 'Api\InterestsController@update');
        Route::get('/interests/delete/{id}', 'Api\InterestsController@delete');

        Route::get('/logs/by_customer/{id}', 'Api\LogsController@getLogByCustomer');
        Route::get('/logs/by_user/{id}', 'Api\LogsController@getLogByUser');
        Route::get('/logs', 'Api\LogsController@getGlobalLog');
    });

    Route::group(['middleware' => 'role:admin|partner'], function() {
        Route::get('/partners', 'Api\PartnersController@getChildren');
        Route::post('/partners/store', 'Api\PartnersController@createChild');
        Route::get('/partners/count', 'Api\PartnersController@countChild');
        Route::get('/partners/show/{id}', 'Api\PartnersController@show');
        Route::post('/partners/update/{id}', 'Api\PartnersController@update');
        Route::get('/partners/delete/{id}', 'Api\PartnersController@delete');
    });

    Route::get('/reminders', 'Api\RemindersController@index');
    Route::get('/reminders/create', 'Api\RemindersController@create');
    Route::get('/reminders/show/{id}', 'Api\RemindersController@show');
    Route::get('/reminders/edit/{id}', 'Api\RemindersController@edit');
    Route::post('/reminders/store', 'Api\RemindersController@store');
    Route::post('/reminders/update/{id}', 'Api\RemindersController@update');
    Route::get('/reminders/delete/{id}', 'Api\RemindersController@delete');
    Route::get('/reminders/today', 'Api\RemindersController@getTodayRemindersCount');
    Route::get('/reminders/done/{id}', 'Api\RemindersController@done');

    Route::get('/auth_user', 'Api\InitController@getAuthUser');
});

Route::get('/translates', 'Api\InitController@translates');


