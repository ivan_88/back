## Start

- `composer install`
- Create database
- Copy `.env.exemple` and rename to `.env`
- Add your database options on `.env`
- `php artisan key:generate`
- `php artisan migrate`
- `php artisan db:seed`

## Commands

- You can use `php artisan project:add {name}` to create new project
- `php artisan route:list --path=api` - list all api routes
- `sudo php phpDocumentor.phar run -d /var/www/bv-crm/app/Http/Controllers/Api -t /var/www/ApiDocs  --title "ApiDocs" --force` - docs

## API

Use `php artisan passport:install` for generate client secret

Use `api/oauth/token` for get access_token. 

- `client_id` - `2`
- `client_secret` - client secret for Password grant client
- `grant_type` - `password`
- `username` - user email
- `password` - user password
- `scope` - `*`

Use header `Authorization` - `Bearer ` + access_token
