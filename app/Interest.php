<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interest extends Model
{
    use SoftDeletes;

    protected $fillable = ['name_en', 'name_uk', 'project_id'];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
        'name_en' => 'required',
        'name_uk' => 'required',
        'project_id' => 'required|exists:projects,id'
    ];

    public function project()
    {
        return $this->belongsTo('App\Project');
    }
}
