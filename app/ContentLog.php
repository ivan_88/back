<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;

class ContentLog extends Model
{
    protected $table = 'contents_log';
    protected $fillable = ['user_id', 'content_id', 'action'];
    public $timestamps = true;

    const ACTION_CREATE = 1;
    const ACTION_UPDATE = 2;
    const ACTION_ARCHIVE = 3;
    const ACTION_ACTIVE = 4;
    const ACTION_ADD_NOTE = 5;

    const ACTION_LEAD_TO_COM = 6;
    const ACTION_COM_TO_NEG = 7;
    const ACTION_NEG_TO_PRO = 8;
    const ACTION_PRO_TO_NEG = 9;
    const ACTION_NEG_TO_COM = 10;
    const ACTION_COM_TO_LEAD = 11;

    /**
     * List actions for moving
     * @var array
     */
    private static $actionsMove = [
        Content::LEAD_TO_COM => self::ACTION_LEAD_TO_COM,
        Content::COM_TO_NEG => self::ACTION_COM_TO_NEG,
        Content::NEG_TO_PRO => self::ACTION_NEG_TO_PRO,
        Content::PRO_TO_NEG => self::ACTION_PRO_TO_NEG,
        Content::NEG_TO_COM => self::ACTION_NEG_TO_COM,
        Content::COM_TO_LEAD => self::ACTION_COM_TO_LEAD,
    ];

    /**
     * Get move action for log
     * @param $key
     * @return mixed
     */
    public static function getMoveAction($key)
    {
        return self::$actionsMove[$key];
    }

    /**
     * Actions keys for translate
     * @var array
     */
    private static $actions = [
    	self::ACTION_CREATE => 'create_customer',
    	self::ACTION_UPDATE => 'update_customer',
    	self::ACTION_ARCHIVE => 'archive',
        self::ACTION_ADD_NOTE => 'add_note',
        self::ACTION_ACTIVE => 'active',
        self::ACTION_LEAD_TO_COM => 'lead_to_com',
        self::ACTION_COM_TO_NEG => 'com_to_neg',
        self::ACTION_NEG_TO_PRO => 'neg_to_pro',
        self::ACTION_PRO_TO_NEG => 'pro_to_neg',
        self::ACTION_NEG_TO_COM => 'neg_to_com',
        self::ACTION_COM_TO_LEAD => 'com_to_lead'
    ];

    /**
     * Return one action label if transmitted $key, else return array labels
     * @param null $key
     * @return array|mixed
     */
    public static function getActions($key = null)
    {
        if ($key) {
            return self::$actions[$key];
        }
    	return self::$actions;
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function customer()
    {
        return $this->belongsTo('App\Content', 'content_id');
    }
}
