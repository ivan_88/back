<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use SoftDeletes, EntrustUserTrait {
        SoftDeletes::restore insteadof EntrustUserTrait;
        EntrustUserTrait::restore insteadof SoftDeletes;
    }

    use HasApiTokens, Notifiable;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Validation rules, for update if transmitted $id, else rules for create
     * @param null $id
     * @return array
     */
    public function getRules($id = null)
    {
        if($id) {
            return [
                'name' => 'required|max:32|string',
                'email' => 'required|max:64|unique:users,email,' . $id . '|email'
            ];
        }
        return [
            'name' => 'required|max:32|string',
            'email' => 'required|max:64|unique:users,email|email',
            'password' => 'required|confirmed|min:6|max:64'
        ];
            
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function parent()
    {
        return $this->belongsTo('App\User', 'parent_id');
    }

    public static function getChildren(int $id)
    {
        return self::with('parent')->where('parent_id', '=', $id)->get();
    }

    public static function getCountChildren(int $id) : int 
    {
        return self::with('parent')->where('parent_id', '=', $id)->count();
    }   
}
