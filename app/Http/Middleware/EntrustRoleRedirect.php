<?php

namespace App\Http\Middleware;

use Closure;
use Zizaco\Entrust\Middleware\EntrustRole;

class EntrustRoleRedirect extends EntrustRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        if (!is_array($roles)) {
            $roles = explode(self::DELIMITER, $roles);
        }

        if ($this->auth->guest() || !$request->user()->hasRole($roles)) {
            return redirect('/');
        }

        return $next($request);

    }
}
