<?php

namespace App\Http\Controllers;

use App\Interest;
use App\Source;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Content;
use App\Country;
use App\Project;
use App\ContentLog;
use App\Note;
use Validator;
use App\Helpers\CacheHelper;
use Illuminate\Support\Facades\Session;
use App\Helpers\ContentHelper;

class ContentsController extends Controller
{

    private $perPage = 10;

    /**
     * View customers list
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->setSession($request);

        $user = Auth::user();

        if ($request->search) {
            $field = array_key_exists($request->field, Content::getSearchFields()) ? Content::getSearchFields($request->field) : 'lastname';
            $contents = Content::where($field, 'like', '%' . $request->search . '%');
        } else {
            $contents = Content::where('type', '=', $request->session()->get('type'))
                ->where('active', '=', $request->session()->get('active'))
                ->where('project_id', '=', $request->session()->get('project'));
        }

        if (!$user->hasRole('manager') && !$user->hasRole('admin')) {
            $contents = $contents->where('user_id', '=', $user->id);
        }

        if ($request->session()->has('interest_id')) {
            $contents = $contents->where('interest_id', '=', $request->session()->get('interest_id'));
        }
        if ($request->session()->has('source_id')) {
            $contents = $contents->where('source_id', '=', $request->session()->get('source_id'));
        }

        if ($request->session()->has('order_by')) {
            $contents = $contents->orderBy($request->session()->get('order_by'), $request->session()->get('order_direction'));
        }

        $contents = $contents->with(['user', 'interest', 'source'])->paginate($this->perPage);

        return view('contents.index', [
            'contents' => $contents,
            'type' => $request->search ? 'Search' : Content::getTypes($request->session()->get('type')),
            'typeId' => $request->session()->get('type'),
            'types' => Content::getTypes(),
            'projects' => CacheHelper::getProjects(),
            'contacts' => Content::getContactsBy(),
            'movingActions' => Content::getActionsByType(),
            'searchFields' => Content::getSearchFields(),
            'sources' => Source::all(),
            'interests' => Interest::where('project_id', '=', $request->session()->get('project'))->get(),
            'name' => Session::has('locale') ? 'name_' . Session::get('locale') : 'name_' . App::getLocale()
        ]);
    }

    /**
     * Show info
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $content = Content::with([
            'country' => function ($query) {
                $query->select('id', 'full_name');
            },
            'interest',
            'project',
            'source',
            'user' => function ($query) {
                $query->select('id', 'name');
            }
        ])
            ->where('id', '=', $id)
            ->first();

        if ($content) {
            return view('contents.info', [
                'content' => $content,
                'contacts' => Content::getContactsBy(),
                'name' => Session::has('locale') ? 'name_' . Session::get('locale') : 'name_' . App::getLocale()
            ]);
        }
        return redirect('404');
    }

    /**
     * View create customer form
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('contents.create', [
            'countries' => CacheHelper::getCountries(),
            'project' => $request->session()->get('project'),
            'types' => Content::getTypes(),
            'projects' => CacheHelper::getProjects(),
            'interests' => CacheHelper::getInterests($request->session()->get('project')),
            'sources' => CacheHelper::getSources(),
            'contacts' => Content::getContactsBy(),
            'name' => $request->session()->has('locale') ? 'name_' . $request->session()->get('locale') : 'name_' . App::getLocale()
        ]);
    }

    /**
     * Store new customer to database
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Content::getRules(null, $request->phone, $request->email));

        if ($validator->fails()) {
            return redirect('/create')
                ->with('data', $request->all())
                ->withErrors($validator)
                ->withInput();
        }

        $content = Content::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone' => $request->phone,
            'email' => $request->email,
            'country_id' => $request->country_id,
            'city' => $request->city,
            'address' => $request->address,
            'zip' => $request->zip,
            'active' => 1,
            'type' => Content::LEAD,
            'user_id' => Auth::user()->id,
            'project_id' => $request->project_id,
            'interest_id' => $request->interest_id,
            'source_id' => $request->source_id,
            'contact_by' => $request->contact_by
        ]);

        if ($content) {
            ContentLog::create([
                'user_id' => Auth::user()->id,
                'content_id' => $content->id,
                'action' => ContentLog::ACTION_CREATE
            ]);

            if ($request->notes != '') {
                Note::create([
                    'text' => $request->notes,
                    'content_id' => $content->id,
                    'user_id' => Auth::user()->id
                ]);
            }
        }

        return redirect('/');
    }

    /**
     * View edit customer form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $content = Content::find($id);
        $user = Auth::user();

        if ((!$user->hasRole('manager') && !$user->hasRole('admin')) && $user->id != $content->user_id) {
            return redirect('/');
        }

        if ($content) {
            return view('contents.edit', [
                'countries' => Country::select('id', 'full_name')->get(),
                'content' => $content,
                'types' => Content::getTypes(),
                'projects' => CacheHelper::getProjects(),
                'interests' => Interest::all(),
                'sources' => Source::all(),
                'contacts' => Content::getContactsBy(),
                'name' => Session::has('locale') ? 'name_' . Session::get('locale') : 'name_' . App::getLocale()
            ]);
        }
        return redirect('404');
    }

    /**
     * Update customer in database
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $content = Content::find($id);
        $user = Auth::user();

        if ((!$user->hasRole('manager') && !$user->hasRole('admin')) && $user->id != $content->user_id) {
            return redirect('/');
        }

        if ($content) {
            $validator = Validator::make($request->all(), Content::getRules($id, $request->phone, $request->email));

            if ($validator->fails()) {
                return redirect('/edit/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $content->firstname = $request->firstname;
            $content->lastname = $request->lastname;
            $content->phone = $request->phone;
            $content->email = $request->email;
            $content->country_id = $request->country_id;
            $content->city = $request->city;
            $content->address = $request->address;
            $content->zip = $request->zip;
            $content->interest_id = $request->interest_id;
            $content->source_id = $request->source_id;
            $content->contact_by = $request->contact_by;

            if ($content->save()) {
                ContentLog::create([
                    'user_id' => Auth::user()->id,
                    'content_id' => $content->id,
                    'action' => ContentLog::ACTION_UPDATE
                ]);

                if ($request->notes != '') {
                    Note::create([
                        'text' => $request->notes,
                        'content_id' => $content->id,
                        'user_id' => $user->id
                    ]);
                }
            }

            return redirect('/');
        }
        return redirect('404');
    }

    /**
     * Change type
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function move(Request $request, $id)
    {
        $content = Content::find($id);

        if ($content) {
            if (!ContentHelper::canUserMove($request->action)) {
                return redirect('403');
            }
            if ($content->move($request->action)) {

                ContentLog::create([
                    'user_id' => Auth::user()->id,
                    'content_id' => $content->id,
                    'action' => ContentLog::getMoveAction($request->action)
                ]);

                return redirect('/');
            }
            return redirect('400');
        }
        return redirect('404');
    }


    /**
     * Archive customer
     * @param \Illuminate\Http\Request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function archiveOrActive(Request $request, $id)
    {
        $content = Content::find($id);
        $validator = Validator::make($request->all(), [
            'reason' => 'required',
            'action' => 'required|in:0,1'
        ]);

        if ($validator->fails()) {
            return response(json_encode(['errors' => $validator->errors()]), 400);
        }

        if ($content) {
            $content->active = $request->action;
            if ($request->action == Content::ARCHIVE) {
                $content->rejection_reason = $request->reason;
            } else {
                $content->cause_of_recovery = $request->reason;
            }

            if ($content->save()) {
                ContentLog::create([
                    'user_id' => Auth::user()->id,
                    'content_id' => $content->id,
                    'action' => $request->action == Content::ARCHIVE ? ContentLog::ACTION_ARCHIVE : ContentLog::ACTION_ACTIVE
                ]);
            }
            return response(json_encode(['success' => true]), 200);
        }
        return response(json_encode(['errors' => ['Not found']]), 404);
    }


    /**
     * View list notes for one customer
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function notes($id)
    {
        $content = Content::find($id);
        if ($content) {
            $notes = Note::with('user')
                ->where('content_id', '=', $id)
                ->paginate($this->perPage);
            return view('contents.notes', ['notes' => $notes, 'content' => $content]);
        }
        return redirect('404');
    }

    /**
     * Add note to the customer
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addNote(Request $request, $id)
    {
        $validator = Validator::make($request->all(), ['note' => 'required']);

        if ($validator->fails()) {
            return response(json_encode(['errors' => $validator->errors()]), 400);
        }

        $user = Auth::user();

        $note = Note::create([
            'user_id' => $user->id,
            'content_id' => $id,
            'text' => $request->note
        ]);

        if ($note) {
            ContentLog::create([
                'user_id' => $user->id,
                'content_id' => $id,
                'action' => ContentLog::ACTION_ADD_NOTE
            ]);
        }

        return response(json_encode(['success' => true]), 200);
    }

    /**
     * Set type, active/archive and project options to session
     * @param Request $request
     */
    private function setSession(Request $request)
    {
        $name = 'name_' . App::getLocale();

        if (isset($request->order_by)) {
            $request->session()->put('order_by', $request->order_by);
            $request->session()->put('order_direction', isset($request->order_direction) ? $request->order_direction : 'asc');
        }

        if (isset($request->interest_id)) {
            $request->session()->put('interest_id', $request->interest_id);
        }

        if (isset($request->source_id)) {
            $request->session()->put('source_id', $request->source_id);
        }

        if ($request->type && Content::checkType($request->type)) {
            $request->session()->put('type', $request->type);
        } elseif (!$request->type && !$request->session()->has('type')) {
            $request->session()->put('type', Content::LEAD);
        }

        if (isset($request->active)) {
            $request->session()->put('active', $request->active);
        } elseif (!isset($request->active) && !$request->session()->has('active')) {
            $request->session()->put('active', 1);
        }

        if ($request->project) {
            $request->session()->put('project', $request->project);
            $request->session()->put('project_name', Project::find($request->project)->$name);
        } elseif (!$request->project && !$request->session()->has('project')) {
            $project = Project::first();
            $request->session()->put('project', $project->id);
            $request->session()->put('project_name', $project->$name);
        } elseif ($request->session()->has('project') && !$request->session()->has('project_name')) {
            $request->session()->put('project_name', Project::find($request->session()->get('project'))->$name);
        }

        if ($request->clear == 1) {
            $request->session()->forget(['order_by', 'order_direction', 'interest_id', 'source_id']);
        }
    }
}
