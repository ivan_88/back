<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use App\Interest;
use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class InterestsController extends Controller
{
    /**
     * View list interests
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('interests.index', [
            'interests' => Interest::with('project')->get(),
            'name' => Session::has('locale') ? 'name_' . Session::get('locale') : 'name_' . App::getLocale()
        ]);
    }

    /**
     * Create new interest
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('interests.create', [
            'projects' => Project::all(),
            'name' => Session::has('locale') ? 'name_' . Session::get('locale') : 'name_' . App::getLocale()
        ]);
    }

    /**
     * Store new interest in database
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Interest::$rules);

        if ($validator->fails()) {
            return redirect('/interests/create')
                ->withErrors($validator)
                ->withInput();
        }
        Interest::create([
            'name_en' => $request->name_en,
            'name_uk' => $request->name_uk,
            'project_id' => $request->project_id
        ]);
        return redirect('/interests');
    }

    /**
     * View edit interest form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $interest = Interest::find($id);

        if ($interest) {
            return view('interests.edit', [
                'interest' => $interest,
                'projects' => Project::all(),
                'name' => Session::has('locale') ? 'name_' . Session::get('locale') : 'name_' . App::getLocale()
            ]);
        }
        return redirect('404');
    }

    /**
     * Update interest in database
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $interest = Interest::find($id);
        if ($interest) {
            $validator = Validator::make($request->all(), Interest::$rules);

            if ($validator->fails()) {
                return redirect('/interests/edit/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $interest->name_en = $request->name_en;
            $interest->name_uk = $request->name_uk;
            $interest->project_id = $request->project_id;
            $interest->save();
            return redirect('/interests');
        }
        return redirect('404');
    }

    /**
     * Soft delete interest
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $interest = Interest::find($id);
        $interest->delete();
        return redirect('/interests');
    }
}
