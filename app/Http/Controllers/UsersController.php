<?php

namespace App\Http\Controllers;

use App\ContentLog;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{
    private $perPage = 10;

    /**
     * View users list
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
    	return view('users.index', [
    	    'users' => User::with('roles')->paginate($this->perPage),
            'authUser' => Auth::user()
        ]);
    }

    /**
     * View create user form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
    	return view('users.create', ['roles' => Role::all()]);
    }

    /**
     * Store new user in database
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function store(Request $request)
    {
    	$user = new User(); 	
    	$validator = Validator::make($request->all(), $user->getRules());

    	if ($validator->fails()) {
      		return redirect('/users/create')
                  ->withErrors($validator)
                  ->withInput();
    	}
    	$user->name = $request->name;
    	$user->email = $request->email;
    	$user->password = bcrypt($request->password);
    	$user->save();
    	if ($user->id) {
    		DB::table('role_user')->insert([
    		    'user_id' => $user->id,
                'role_id' => $request->role_id ? $request->role_id : Role::BROKER
            ]);
    		return redirect('/users');
    	} else {
    		throw new \Exception("User not save", 1);
    	}
    }

    /**
     * View edit user form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id = null)
    {
        $user = User::find($id);

    	if ($user) {
    		return view('users.edit', ['user' => $user, 'roles' => Role::all()]);
    	}
    	return redirect('404');
    }

    /**
     * Update user in database
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) 
    {
    	$user = User::find($id);
    	$authUser = Auth::user();

    	if (!$authUser->hasRole('admin') && ($user->hasRole('manager') || $user->hasRole('admin')) && $user->id != $authUser->id) {
            return redirect('403');
        }

    	if ($user) {
    		$validator = Validator::make($request->all(), $user->getRules($id));

    		if ($validator->fails()) {
      			return redirect('/users/edit/' . $id)
                  	->withErrors($validator)
                  	->withInput();
    		}

    		$user->name = $request->name;
    		$user->email = $request->email;
    		$user->save();
            if ($user->role_id != $request->role_id) {
                DB::table('role_user')
                    ->where('user_id', '=', $user->id)
                    ->update(['user_id' => $user->id, 'role_id' => $request->role_id]);
                return redirect('/users');
            }
    		return redirect('/users');
    	}
        return redirect('404');
    }

    /**
     * Soft delete user
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
    	$user = User::find($id);
    	$user->roles()->detach();
    	$user->delete();
    	return redirect('/users');
    }


    /**
     * Show edit user form for auth user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function editSelf()
    {
        $user = Auth::user();
        if (!$user) {
            return redirect('/login');
        }
        return view('users.edit', ['user' => $user, 'roles' => Role::all()]);
    }

    /**
     * Show change password form for auth user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function changePasswordForm()
    {
        $user = Auth::user();
        if (!$user) {
            return redirect('/login');
        }
        return view('users.password', ['user' => $user]);
    }

    /**
     * Change password for auth user
     * @param Request $request
     * @return $this
     */
    public function changePassword(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), ['password' => 'required|confirmed|min:6|max:64|regex:/\S/']);

        if ($validator->fails()) {
            return redirect('/users/password')
                ->withErrors($validator)
                ->withInput();
        }

        if (Auth::validate(['email' => $user->email, 'password' => $request->old_password])) {
            $user->password = bcrypt($request->password);
            $user->save();
            Session::flash('success', 'Password changed');
            return redirect('/users/password');
        }
        return redirect('/users/password')
            ->withErrors(['old_password' => 'Old password is wrong'])
            ->withInput();
    }
}
