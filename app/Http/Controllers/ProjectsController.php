<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Validator;

class ProjectsController extends Controller
{
    /**
     * View projects list
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('projects.index', ['projects' => Project::all()]);
    }

    /**
     * View create project form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store new project in database
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Project::$rules);

        if ($validator->fails()) {
            return redirect('/projects/create')
                ->withErrors($validator)
                ->withInput();
        }
        Project::create([
            'name_en' => $request->name_en,
            'name_uk' => $request->name_uk
        ]);
        return redirect('/projects');
    }

    /**
     * View edit project form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $project = Project::find($id);

        if ($project) {
            return view('projects.edit', ['project' => $project]);
        }
        return redirect('404');
    }

    /**
     * Update project in database
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        if ($project) {
            $validator = Validator::make($request->all(), Project::$rules);

            if ($validator->fails()) {
                return redirect('/projects/edit/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $project->name_en = $request->name_en;
            $project->name_uk = $request->name_uk;
            $project->save();
            return redirect('/projects');
        }
        return redirect('404');
    }

    /**
     * Soft delete projects
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $project = Project::find($id);
        $project->delete();
        return redirect('/projects');
    }
}
