<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Content;
use App\ContentLog;
use App\User;

class LogsController extends Controller
{

    private $perPage = 20;
    private $start;
    private $end;

    /**
     * View log events for one customer
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function viewLogByCustomer(Request $request, $id)
    {
        $content = Content::find($id);
        if (!$content) {
            return redirect('404');
        }

        $logs = ContentLog::with('user')
            ->where('content_id', '=', $id);

        if (isset($request->excel) && $request->excel == 1) {
            $logs = $logs->get();
            $this->makeExcel(
                $logs,
                ContentLog::getActions(),
                'logs.log_by_customer_table',
                'Logs:_' . $content->firstname . '_' . $content->lastname);
        } else {
            $logs = $logs->paginate($this->perPage);
            return view('logs.log_by_customer', [
                'content' => $content,
                'log' => $logs,
                'actions' => ContentLog::getActions()
            ]);
        }
    }

    /**
     * View logs for one user
     * @param Request $request
     * @param $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function viewLogByUser(Request $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return redirect('404');
        }
        $logs = ContentLog::where('user_id', '=', $id)
            ->with(['content' => function ($query) {
                $query->select('firstname', 'lastname', 'id', 'rejection_reason', 'cause_of_recovery');
            }]);

        if (isset($request->excel) && $request->excel == 1) {
            $logs = $logs->get();
            $this->makeExcel($logs, ContentLog::getActions(), 'logs.log_by_user_table', 'Logs:_' . $user->name);
        } else {
            $logs = $logs->paginate($this->perPage);

            return view('logs.log_by_user', [
                'log' => $logs,
                'actions' => ContentLog::getActions(),
                'user' => $user
            ]);
        }
    }


    /**
     * View all logs with datetime filter
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewGlobalLog(Request $request)
    {
        $this->setSession($request);

        $log = ContentLog::with(['user', 'content']);
        if ($this->start) {
            $log = $log->where('created_at', '>=', $this->start);
        }
        if ($this->end) {
            $log = $log->where('created_at', '<=', $this->end);
        }
        if (isset($request->excel) && $request->excel == 1) {
            $log = $log->get();
            $this->makeExcel($log, ContentLog::getActions(), 'logs.global_log_table', 'Global_log');
        } else {
            $log = $log->paginate($this->perPage);
            return view('logs.global_log', ['log' => $log, 'actions' => ContentLog::getActions()]);
        }
    }

    /**
     * Set session variables
     * @param Request $request
     */
    private function setSession(Request $request)
    {
        if (isset($request->start)) {
            $this->start = date('Y.m.d H:i:s', strtotime($request->start));
            $request->session()->put('start', $this->start);
        } else {
            $this->start = $request->session()->has('start') ? $request->session()->get('start') : false;
        }
        if (isset($request->end)) {
            $this->end = date('Y.m.d H:i:s', strtotime($request->end));
            $request->session()->put('end', $this->end);
        } else {
            $this->end = $request->session()->has('end') ? $request->session()->get('end') : false;
        }

        if (isset($request->clear) && $request->clear == 1) {
            $request->session()->forget(['start', 'end']);
            $this->start = false;
            $this->end = false;
        }
    }

    /**
     * Make excel file
     * @param Collection $data
     * @param array $actions
     * @param string $view
     * @param string $name
     * @return void
     */
    private function makeExcel(Collection $data, array $actions, string $view, string $name)
    {
        $excel = App::make('excel');
        $excel->create($name, function ($excel) use ($data, $actions, $view) {
            $excel->sheet('log', function ($sheet) use ($data, $actions, $view) {
                $sheet->loadView($view, ['log' => $data, 'actions' => $actions]);
            });
            $excel->store('xls')
                ->export('xls');
        });
    }
}
