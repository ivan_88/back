<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Content;

class StatisticController extends Controller
{

    /**
     * View statistics 
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function statistic(Request $request)
    {
        $statistics = DB::table('contents')
            ->select('users.name', DB::raw('count(contents.id) as count'))
            ->leftJoin('users', 'contents.user_id', '=', 'users.id');
        if (isset($request->type)) {
            $statistics = $statistics->where('contents.type', '=', $request->type);
        }
        $statistics = $statistics->groupBy('contents.user_id')
            ->get();

        return view('contents.statistic', [
            'statistics' => $statistics,
            'type' => $request->type,
            'types' => Content::getTypes()
        ]);
    }
}
