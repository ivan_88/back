<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;
use App\Reminder;
use Illuminate\Support\Facades\Auth;
use Validator;

class RemindersController extends Controller
{
    private $perPage = 20;

    public function index(Request $request)
    {
        $this->setSession($request);
        list($today, $tomorrow) = $this->getTodayAndTomorrow();

        $reminders = Reminder::with('content')
            ->where('user_id', '=', Auth::user()->id)
            ->where('done', '=', $request->session()->get('executed'));
        if ($request->session()->has('today') && $request->session()->get('today') == 1) {
            $reminders = $reminders ->where('assigned_to', '>=', $today)
                ->where('assigned_to', '<=', $tomorrow);
        }
        $reminders = $reminders->paginate($this->perPage);

        return view('reminders.index', ['reminders' => $reminders]);
    }

    public function create()
    {
        $user = Auth::user();

        if (!$user->hasRole('admin') && !$user->hasRole('manager') ) {
            $contents = Content::where('user_id', '=', $user->id)->get();
        } else {
            $contents = Content::all();
        }

        return view('reminders.create', ['contents' => $contents]);
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Reminder::$rules);

        if ($validator->fails()) {
            return redirect('/reminders/create')
                ->withErrors($validator)
                ->withInput();
        }

        Reminder::create([
            'content_id' => $request->content_id,
            'user_id' => Auth::user()->id,
            'note' => $request->note,
            'assigned_to' => date('Y-m-d H:i:s', strtotime($request->assigned_to))
        ]);

        return redirect('/reminders');
    }

    /**
     * View reminder
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function show($id)
    {
        $reminder = Reminder::with('content')
            ->where('id', '=', $id)
            ->first();

        if ($reminder) {
            return view('reminders.view', ['reminder' => $reminder]);
        }
        return redirect('404');
    }

    /**
     * View edit reminder form
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function edit($id)
    {
        $reminder = Reminder::find($id);
        $user = Auth::user();

        if (!$user->hasRole('admin') && !$user->hasRole('manager') ) {
            $contents = Content::where('user_id', '=', $user->id)->get();
        } else {
            $contents = Content::all();
        }

        if ($reminder) {
            return view('reminders.edit', ['reminder' => $reminder, 'contents' => $contents]);
        }
        return redirect('404');
    }

    /**
     * Update reminder in database
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $project = Reminder::find($id);
        if ($project) {
            $validator = Validator::make($request->all(), Reminder::$rules);

            if ($validator->fails()) {
                return redirect('/reminders/edit/' . $id)
                    ->withErrors($validator)
                    ->withInput();
            }

            $project->content_id = $request->content_id;
            $project->note = $request->note;
            $project->assigned_to = date('Y-m-d H:i:s', strtotime($request->assigned_to));
            $project->save();
            return redirect('/reminders');
        }
        return redirect('404');
    }

    /**
     * Delete reminders
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function delete($id)
    {
        $reminder = Reminder::find($id);
        $reminder->delete();
        return redirect('/reminders');
    }


    /**
     * Get counts reminders for current day for auth user
     * @return string
     */
    public function getTodayRemindersCount()
    {
        list($today, $tomorrow) = $this->getTodayAndTomorrow();

        $reminders = Reminder::where('user_id', '=', Auth::user()->id)
            ->where('done', '=', 0)
            ->where('assigned_to', '>=', $today)
            ->where('assigned_to', '<=', $tomorrow)
            ->get();

        return response(json_encode([
            'list' => $reminders,
            'count' => $reminders->count()
        ]), 200);
    }

    /**
     * Set done field in database
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function done(Request $request, $id)
    {
        $reminder = Reminder::find($id);
        if ($reminder) {
            $reminder->done = isset($request->done) && $request->done == 1 ? 1 :0;
            $reminder->save();
            return response(json_encode(['success' => true]), 200);
        }
        return response(json_encode(['fail' => true]), 400);
    }

    /**
     * Get formatted dates today and tomorrow
     * @return array
     */
    private function getTodayAndTomorrow() : array
    {
        $today = date('Y-m-d');
        $todayTimistamp = date('U', strtotime($today));
        $tomorrow = date('Y-m-d H:i:s', $todayTimistamp + 3600 * 24);

        return [$today, $tomorrow];
    }

    /**
     * Set options to session
     * @param Request $request
     */
    private function setSession(Request $request)
    {
        if (isset($request->done)) {
            $request->session()->put('executed', $request->done);
        }

        if (isset($request->today)) {
            $request->session()->put('today', $request->today);
        }
    }
}
