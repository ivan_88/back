<?php
/**
 * Reminders controller
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Reminder;
use App\Content;
use Illuminate\Support\Facades\Auth;
use Validator;

/**
 * Class RemindersController
 */
class RemindersController extends Controller
{
    /**
     * Count records in list
     *
     * @var int
     */
    private $perPage = 20;

    /**
     * List reminders for auth user
     *
     * Get parameter "executed" is required, must be equal 0 or 1
     * Get parameter "today" is optional, must be equal 1
     *
     * @api
     * @Rest\Get("api/reminders")
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        list($today, $tomorrow) = $this->getTodayAndTomorrow();
        return response()->json(['reminders' => Reminder::getList(Auth::user()->id, $request, $today, $tomorrow, $this->perPage)], 200);
    }

    /**
     * Get list customers for auth user
     *
     * @api
     * @Rest\Get("api/reminders/create")
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $user = Auth::user();
        if (!$user->hasRole('admin') && !$user->hasRole('manager') ) {
            $customers = Content::where('user_id', '=', $user->id)->get();
        } else {
            $customers = Content::all();
        }

        return response()->json(['customers' => $customers], 200);
    }

    /**
     * Store new reminder in database
     *
     * Fields:
     *      "content_id" - required
     *      "note" - required
     *      "assigned_to" -required, must be valid date format "Y-m-h H:i:s"
     *
     * @api
     * @Rest\Post("api/reminders/store")
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Reminder::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $reminder = Reminder::create([
            'content_id' => $request->content_id,
            'user_id' => Auth::user()->id,
            'note' => $request->note,
            'assigned_to' => date('Y-m-d H:i:s', strtotime($request->assigned_to))
        ]);

        if ($reminder) {
            return response()->json(['success' => true, 'reminder' => $reminder], 200);
        }
        return response()->json(['errors' => ['Reminder not create']], 400);
    }

    /**
     * Get one reminder
     *
     * @api
     * @Rest\Post("api/reminders/show/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $reminder = Reminder::with('content')
            ->where('id', '=', $id)
            ->first();

        if ($reminder) {
            return response()->json(['reminder' => $reminder]);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Get data for edit reminder form
     *
     * @api
     * @Rest\Get("api/reminders/edit/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        $reminder = Reminder::with('content')->find($id);
        $user = Auth::user();

        if (!$user->hasRole('admin') && !$user->hasRole('manager') ) {
            $customers = Content::where('user_id', '=', $user->id)->get();
        } else {
            $customers = Content::all();
        }

        if ($reminder) {
            return response()->json(['reminder' => $reminder, 'customers' => $customers]);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Update reminder in database
     *
     * Fields:
     *      "content_id" - required
     *      "note" - required
     *      "assigned_to" -required, must be valid date format "Y-m-h H:i:s"
     *
     * @api
     * @Rest\Post("api/reminders/update/{id}")
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $reminder = Reminder::find($id);
        if ($reminder) {
            $validator = Validator::make($request->all(), Reminder::$rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            $reminder->fill($request->all());
            $reminder->assigned_to = date('Y-m-d H:i:s', strtotime($request->assigned_to));
            $reminder->save();
            return response()->json(['success' => true, 'reminder' => $reminder], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Delete reminders
     *
     * @api
     * @Rest\Get("api/reminders/delete/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $reminder = Reminder::find($id);
        if ($reminder) {
            $reminder->delete();
            return response()->json(['success' => true], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Get reminders and count reminders for current day for auth user
     *
     * @api
     * @Rest\Get("api/reminders/today")
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTodayRemindersCount()
    {
        list($today, $tomorrow) = $this->getTodayAndTomorrow();

        $reminders = Reminder::getTodayRemindersCount(Auth::user()->id, $today, $tomorrow);

        return response()->json([
            'list' => $reminders,
            'count' => $reminders->count()
        ], 200);
    }

    /**
     * Set done field in database
     *
     * Get parameter "done" is required and must be equal 1 or 0
     *
     * @api
     * @Rest\Get("api/reminders/done/{id}")
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function done(Request $request, $id)
    {
        $reminder = Reminder::find($id);
        if ($reminder) {
            $reminder->done = isset($request->done) && $request->done == 1 ? 1 : 0;
            $reminder->save();
            return response()->json(['success' => true, 'reminder' => $reminder], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Get formatted dates today and tomorrow
     *
     * @return array
     */
    private function getTodayAndTomorrow() : array
    {
        $today = date('Y-m-d');
        $todayTimistamp = date('U', strtotime($today));
        $tomorrow = date('Y-m-d H:i:s', $todayTimistamp + 3600 * 24);

        return [$today, $tomorrow];
    }
}
