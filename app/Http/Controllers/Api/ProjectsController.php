<?php
/**
 * Projects controller
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Project;
use Validator;
use Illuminate\Support\Facades\Cache;

/**
 * Class ProjectsController
 */
class ProjectsController extends Controller
{
    /**
     * Get projects list
     *
     * @api
     * @Rest\Get("api/projects")
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['projects' => Project::all()], 200);
    }

    /**
     * Store new project in database
     *
     * Fields:
     *      "name_en" - required
     *      "name_uk" - required
     *
     * @param Request $request
     * @api
     * @Rest\Post("api/projects/store")
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Project::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }
        $project = Project::create($request->all());
        if ($project) {
            return response()->json([
                'success' => true,
                'project' => $project
            ], 200);
        }
        return response()->json(['errors' => ['Project not save']], 400);
    }

    /**
     * Show one project
     *
     * @api
     * @Rest\Get("api/projects/show/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $project = Project::find($id);
        if ($project) {
            return response()->json(['success' => true, 'project' => $project], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Update project in database
     *
     * Fields:
     *      "name_en" - required
     *      "name_uk" - required
     *
     * @param Request $request
     * @param $id
     * @api
     * @Rest\Post("api/projects/update/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $project = Project::find($id);
        if ($project) {
            $validator = Validator::make($request->all(), Project::$rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }
            
            $project->fill($request->all());
            $project->save();
            return response()->json(['success' => true, 'project' => $project], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Soft delete projects
     *
     * @api
     * @Rest\Get("api/projects/delete/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $project = Project::find($id);
        if ($project) {
            $project->delete();
            Cache::forget('projects');
            return response()->json(['success' => true, 'project' => $project], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }
}
