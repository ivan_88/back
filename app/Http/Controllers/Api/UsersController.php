<?php
/**
 * Users controller
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

/**
 * Class UsersController
 */
class UsersController extends Controller
{
    /**
     * Count records in list
     *
     * @var int
     */
    private $perPage = 10;

    /**
     * Get users list. Only for admin and manager
     *
     * @api
     * @Rest\Get("api/users")
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json([
            'users' => User::with('roles')->paginate($this->perPage)
        ], 200);
    }

    /**
     * Store new user in database
     *
     * Fields:
     *      "name" - required
     *      "email" - required, unique, must be a valid email address
     *      "password" - required
     *      "password_confirmation" - required, must be equal "password"
     *      "role_id" - optional, enable only for admin
     *
     * @param Request $request
     * @api
     * @Rest\Post("api/users/store")
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $user = new User();
        $authUser = Auth::user();
        $validator = Validator::make($request->all(), $user->getRules());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->parent_id = $authUser->id;
        if ($request->role_id && $request->role_id == Role::PARTNER) {
            $user->level = 1;
        }
        $user->save();
        if ($user->id) {
            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => $request->role_id && Auth::user()->hasRole('admin') ? $request->role_id : Role::BROKER
            ]);
            return response()->json([
                'success' => true,
                'user' => User::with('roles')->find($user->id)
            ], 200);
        } else {
            throw new \Exception("User not save", 1);
        }
    }

    /**
     * Get one user
     *
     * @param $id
     * @api
     * @Rest\Get("api/users/show")
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id = null)
    {
        $user = User::with('roles')->find($id);
        $authUser = Auth::user();

        if ($user) {
            if ($this->canSee($authUser, $user)) {
                return response()->json(['user' => $user], 200);
            }
            return response()->json(['errors' => ['403']], 403);
        }

        return response()->json(['errors' => ['404']], 404);
    }


    /**
     * Update user in database
     *
     * Fields:
     *      "name" - required
     *      "email" - required, unique, must be a valid email address
     *      "password" - required
     *      "password_confirmation" - required, must be equal "password"
     *      "role_id" - optional, enable only for admin
     *
     * @param Request $request
     * @param $id
     * @api
     * @Rest\Post("api/users/update/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $authUser = Auth::user();

        if (!$authUser->hasRole('admin') && ($user->hasRole('manager') || $user->hasRole('admin')) && $user->id != $authUser->id) {
            return response()->json(['errors' => ['403']], 403);
        }

        if ($user) {
            $validator = Validator::make($request->all(), $user->getRules($id));

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            $user->fill($request->all());
            $user->save();
            if ($user->role_id != $request->role_id && $authUser->hasRole('admin')) {
                DB::table('role_user')
                    ->where('user_id', '=', $user->id)
                    ->update(['user_id' => $user->id, 'role_id' => $request->role_id]);
            }
            return response()->json([
                'success' => true,
                'user' => User::with('roles')->find($user->id)
            ], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Soft delete user
     *
     * @param $id
     * @api
     * @Rest\Get("api/users/delete/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $user = User::find($id);
        if ($user) {
            $user->roles()->detach();
            $user->delete();
            return response()->json(['success' => true, 'user' => $user], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }


    /**
     * Show change password form for auth user
     *
     * @api
     * @Rest\Post("api/users/password")
     * @param Request $request
     * @return $this
     */
    public function changePassword(Request $request)
    {
        $user = Auth::user()->select('id', 'email', 'password')->get();
        $validator = Validator::make($request->all(), ['password' => 'required|confirmed|min:6|max:64|regex:/\S/']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        // TODO: fix
        if (Auth::validate(['email' => $user->email, 'password' => $request->old_password])) {
            $user->password = bcrypt($request->password);
            $user->save();
            return response()->json(['success' => true], 200);
        }
        return response()->json(['errors' => [['old_password' => 'Old password is wrong']]], 400);
    }

    private function canSee(User $authUser, User $user) : bool
    {
        if ($authUser->hasRole('admin') || $authUser->hasRole('manager') || ($authUser->hasRole('partner') && $authUser->id === $user->parent_id)) {
            return true;
        }
        return false;
    }
}
