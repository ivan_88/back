<?php
/**
 * Interests controller
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interest;
use Validator;
use Illuminate\Support\Facades\Cache;

/**
 * Class InterestsController
 */
class InterestsController extends Controller
{
    /**
     * Get interest list
     *
     * @api
     * @Rest\Get("api/interests")
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['interests' => Interest::with('project')->get()], 200);
    }

    /**
     * Store new interest in database
     *
     * Fields:
     *      "name_en" - required
     *      "name_uk" - required
     *      "project_id" - required
     *
     * @param Request $request
     * @api
     * @Rest\Post("api/interests/store")
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Interest::$rules);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }
        $interest = Interest::create($request->all());
        if ($interest) {
            return response()->json([
                'success' => true,
                'interest' => $interest
            ], 200);
        }
        return response()->json(['errors' => ['Interest not save']], 400);
    }

    /**
     * Show one interest
     *
     * @api
     * @Rest\Get("api/interests/show/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $interest = Interest::find($id);
        if ($interest) {
            return response()->json(['success' => true, 'interest' => $interest], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Update interest in database
     *
     * Fields:
     *      "name_en" - required
     *      "name_uk" - required
     *      "project_id" - required
     *
     * @param Request $request
     * @param $id
     * @api
     * @Rest\Post("api/interests/update/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $interest = Interest::find($id);

        if ($interest) {
            $validator = Validator::make($request->all(), Interest::$rules);

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }
            $interest->fill($request->all());
            $interest->save();
            return response()->json(['success' => true, 'interest' => $interest], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Soft delete interest
     *
     * @api
     * @Rest\Get("api/interests/delete/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $interest = Interest::find($id);
        if ($interest) {
            $interest->delete();
            Cache::forget('interests');
            return response()->json(['success' => true, 'interest' => $interest], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }
}
