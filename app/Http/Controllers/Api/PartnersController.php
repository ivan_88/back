<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;
use App\User;
use App\Role;

class PartnersController extends Controller
{

    /**
     * The numbers of referrals can be created by the user
     * Key - level, value count
     *
     * @var array
     */
    private static $levelsChildren = [1 => 4, 2 => 3, 3 => 2, 4 => 0];

    /**
     * Get referrals for auth user
     *
     * @api
     * @Rest\Get("api/partners")
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChildren()
    {
        return response()->json(['users' => User::getChildren(Auth::user()->id)], 200);
    }

    /**
     * Create new referral
     *
     * @api
     * @Rest\Post("api/partners/create")
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function createChild(Request $request)
    {
        $authUser = Auth::user();

        if (!$this->canUserCreateChild($authUser)) {
            return response()->json(['errors' => ['Limit']], 400);
        }

        $user = new User();
        $validator = Validator::make($request->all(), $user->getRules());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user->fill($request->all());
        $user->password = bcrypt($request->password);
        $user->parent_id = $authUser->id;
        $user->level = $authUser->hasRole('admin') ? 1 : $authUser->level + 1;
        $user->save();

        if ($user->id) {
            DB::table('role_user')->insert([
                'user_id' => $user->id,
                'role_id' => Role::PARTNER
            ]);
            return response()->json(['user' => User::with('roles')->find($user->id)], 200);
        } else {
            throw new \Exception("User not save", 1);
        }
    }


    /**
     * Update referral
     *
     * @api
     * @Rest\Post("api/partners/update/{id}")
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response()->json(['errors' => ['404']], 404);
        }

        if (!$this->canUpdate($user)) {
            return response()->json(['errors' => ['403']], 403);
        }

        $validator = Validator::make($request->all(), $user->getRules($id));

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user->fill($request->all());
        $user->save();

        return response()->json([
            'user' => User::with('roles')->find($user->id)
        ], 200);
        
    }

    /**
     * Get count created referrals and count referrals can be created
     *
     * @api
     * @Rest\Get("api/partners/count")
     * @return \Illuminate\Http\JsonResponse
     */
    public function countChild()
    {
        $authUser = Auth::user();
        $count = User::getCountChildren($authUser->id);

        return response()->json([
            'created' => $count,
            'canCreate' => self::canCreate((int)$authUser->level, $count)
        ]);
    }

    /**
     * Get one user and him referrals
     *
     * @api
     * @Rest\Get("api/partners/show/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $user = User::with(['roles', 'parent'])->find($id);
        $children = User::where('parent_id', '=', $user->id)->get();

        if ($user) {
            return response()->json(['user' => $user, 'children' => $children], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Soft delete referral
     *
     * @api
     * @Rest\Get("api/partners/delete/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $user = User::find($id);

        if (!$this->canUpdate($user)) {
            return response()->json(['errors' => ['403']], 403);
        }

        if ($user) {
            $user->roles()->detach();
            $user->delete();
            return response()->json(['user' => $user], 200);
        }

        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Check can it be create referral
     *
     * @param $authUser
     * @return bool
     */
    private function canUserCreateChild($authUser) : bool
    {
        if ($authUser->hasRole('admin')) {
            return true;
        }

        $count = User::with('parent')
            ->where('parent_id', '=', $authUser->id)
            ->count();
        $level = (int)$authUser->level;

        if (array_key_exists($level, self::$levelsChildren) && $count < self::$levelsChildren[$level]) {
            return true;
        }
        return false;
    }

    private function canUpdate(User $user) : bool
    {
        $authUser = Auth::user();

        if ($authUser->hasRole('admin') || $user->parent_id === $authUser->id) {
            return true;
        }
        return false;
    }

    private static function canCreate(int $level, int $count) : int
    {
        return array_key_exists($level, self::$levelsChildren) ? self::$levelsChildren[$level] - $count : 0;
    }
}