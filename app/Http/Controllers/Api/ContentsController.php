<?php
/**
 * Customers controller
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Content;
use App\ContentLog;
use App\Note;
use App\Helpers\ContentHelper;
use Auth;
use Validator;

/**
 * Class ContentsController
 */
class ContentsController extends Controller
{
    /**
     * Count records in list
     *
     * @var int
     */
    private $perPage = 10;

    /**
     * Get customers list
     *
     *   Get parameters:
     *      required except search: "type", "active", "project"
     *      optional: "search", "field", "interest_id", "source_id", "order_by", "order_direction"
     *
     * @api
     * @Rest\Get("api/customers")
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        return response()->json(['data' => Content::getList($request, Content::getApp($request->app), Auth::user(), $this->perPage)], 200);
    }

    /**
     * Get info for one customer
     *
     * @api
     * @Rest\Get("api/customers/show/{id}")
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $customer = Content::getOne($id);

        if ($customer) {
            $user = Auth::user();
            $this->checkRole($user, $customer);
            return response()->json(['data' => $customer], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }


    /**
     * Store new customer to database
     *
     *  Fields:
     *      "firstname" - required
     *      "lastname" - required
     *      "phone" - required, if "email" empty
     *      "email" - required, if "phone" empty
     *      "project_id" - required
     *      "interest_id" - required
     *      "source_id" - required
     *      "contact_by" - required
     *      "country_id" - optional
     *      "city" - optional
     *      "address" - optional
     *      "zip" - optional
     *      "note" - optional
     *
     * @api
     * @Rest\Post("api/customers/store")
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), Content::getRules(null, $request->phone, $request->email));

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user = Auth::user();

        $customer = Content::create([
            'firstname' => $request->firstname,
            'lastname' => $request->lastname,
            'phone' => $request->phone,
            'email' => $request->email,
            'country_id' => $request->country_id,
            'city' => $request->city,
            'address' => $request->address,
            'zip' => $request->zip,
            'active' => 1,
            'type' => Content::LEAD,
            'user_id' => $user->id,
            'project_id' => $request->project_id,
            'interest_id' => $request->interest_id,
            'source_id' => $request->source_id,
            'contact_by' => $request->contact_by,
            'app' => Content::getApp($request->app) // Костыль
        ]);

        if ($customer) {
            ContentLog::create([
                'user_id' => $user->id,
                'content_id' => $customer->id,
                'action' => ContentLog::ACTION_CREATE
            ]);

            if ($request->note != '') {
                Note::create([
                    'text' => $request->note,
                    'content_id' => $customer->id,
                    'user_id' => $user->id
                ]);
            }
        }

        return response()->json(['success' => true, 'customer' => $customer], 200);
    }


    /**
     * Update customer in database
     *
     *  Fields:
     *      "firstname" - required
     *      "lastname" - required
     *      "phone" - required, if "email" empty
     *      "email" - required, if "phone" empty
     *      "project_id" - required
     *      "interest_id" - required
     *      "source_id" - required
     *      "contact_by" - required
     *      "country_id" - optional
     *      "city" - optional
     *      "address" - optional
     *      "zip" - optional
     *
     * @param Request $request
     * @param $id
     * @api
     * @Rest\Post("api/customers/update/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $customer = Content::find($id);
        $user = Auth::user();

        $this->checkRole($user, $customer);

        if ($customer) {
            $validator = Validator::make($request->all(), Content::getRules($id, $request->phone, $request->email));

            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()], 400);
            }

            $customer->fill($request->all());

            if ($customer->save()) {
                ContentLog::create([
                    'user_id' => Auth::user()->id,
                    'content_id' => $customer->id,
                    'action' => ContentLog::ACTION_UPDATE
                ]);

                if ($request->note != '') {
                    Note::create([
                        'text' => $request->note,
                        'content_id' => $customer->id,
                        'user_id' => $user->id
                    ]);
                }
            }

            return response()->json(['success' => true, 'customer' => $customer], 200);
        }
        return response()->json(['errors' => ['404']], 404);
    }


    /**
     * Change type
     *
     * Get parameter 'action' in required
     *
     * @param Request $request
     * @param $id
     * @api
     * @Rest\Get("api/customers/move/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function move(Request $request, $id)
    {
        $customer = Content::find($id);

        if ($customer) {
            if (!ContentHelper::canUserMove($customer->user_id, $request->action)) {
                return response()->json(['errors' => ['403']], 403);
            }
            if ($customer->move($request->action)) {

                ContentLog::create([
                    'user_id' => Auth::user()->id,
                    'content_id' => $customer->id,
                    'action' => ContentLog::getMoveAction($request->action)
                ]);

                return response()->json(['success' => true, 'customer' => $customer], 200);
            }
            return response()->json(['errors' => ['Wrong action']], 400);
        }
        return response()->json(['errors' => ['404']], 404);
    }


    /**
     * Archive or activate customer
     *
     * Fields "action" and "reason" is required
     *
     * @param \Illuminate\Http\Request
     * @param $id
     * @Rest\Post("api/customers/active/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function archiveOrActive(Request $request, $id)
    {
        $customer = Content::find($id);
        $validator = Validator::make($request->all(), Content::getRulesForArchiveOrActive());

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $user = Auth::user();

        if ($customer) {
            $this->checkRole($user, $customer);
           
            $customer->active = $request->action;
            if ($request->action == Content::ARCHIVE) {
                $customer->rejection_reason = $request->reason;
            } else {
                $customer->cause_of_recovery = $request->reason;
            }

            if ($customer->save()) {
                ContentLog::create([
                    'user_id' => $user->id,
                    'content_id' => $customer->id,
                    'action' => $request->action == Content::ARCHIVE ? ContentLog::ACTION_ARCHIVE : ContentLog::ACTION_ACTIVE
                ]);
            }
            return response()->json(['success' => true, 'customer' => $customer], 200);
          
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Get list notes for one customer
     *
     * @param $id
     * @Rest\Get("api/customers/notes/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function notes($id)
    {
        $content = Content::find($id);
        if ($content) {
            return response()->json(['notes' => Note::getList($id, $this->perPage)]);
        }
        return response()->json(['errors' => ['404']], 404);
    }

    /**
     * Add note to the customer
     *
     * Field "note" is required
     *
     * @Rest\Post("api/customers/note/{id}")
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addNote(Request $request, $id)
    {
        if (!Content::find($id)) {
            return response()->json(['errors' => ['404']], 404);
        }

        $validator = Validator::make($request->all(), ['note' => 'required']);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()], 400);
        }

        $note = Note::create([
            'user_id' => Auth::user()->id,
            'content_id' => $id,
            'text' => $request->note
        ]);

        if ($note) {
            return response()->json(['success' => true, 'note' => $note], 200);
        }
        throw new \Exception('Note not created');
    }


    /**
     * Check user permission
     *
     * @param $user
     * @param $customer
     * @return \Illuminate\Http\JsonResponse
     */
    private function checkRole($user, $customer)
    {
        if (!$user->hasRole('admin') && !$user->hasRole('manager') && $user->id != $customer->user_id) {
            return response()->json(['errors' => ['403']], 403);
        }
    }
}
