<?php
/**
 * Init controller
 *
 * Get main application data
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Country;
use App\User;
use App\Content;
use App\Interest;
use App\Source;
use App\Role;
use App\ContentLog;
use App\Helpers\CacheHelper;

/**
 * Class InitController
 *
 */
class InitController extends Controller
{
    /**
     * Get main crm info
     *
     * @api
     * @Rest\Get("api/init")
     * @return \Illuminate\Http\JsonResponse
     */
    public function init()
    {
        return response()->json([
            'countries' => Country::select('id', 'full_name')->get(),
            'types' => Content::getTypes(),
            'projects' => CacheHelper::getProjects(),
            'interests' => Interest::all(),
            'sources' => Source::all(),
            'contacts' => Content::getContactsBy(),
            'roles' => Role::all(),
            'actions' => ContentLog::getActions(),
            'searchFields' => Content::getSearchFields()
        ], 200);
    }

    /**
     * Get translates
     *
     * @api
     * @Rest\Get("api/translates")
     * @return \Illuminate\Http\JsonResponse
     */
    public function translates()
    {
        $translates = [];

        $dirs = scandir(resource_path('lang'));

        foreach ($dirs as $dir) {
            if ($dir != '.' && $dir != '..') {
                $files = scandir(resource_path('lang/' . $dir));
                foreach ($files as $file) {
                    if ($file != '.' && $file != '..') {
                        $filename = explode('.', $file);
                        $translates[$dir][$filename[0]] = include(resource_path('lang/' . $dir . '/' . $file));
                    }
                }
            }
        }
        return response()->json(['translates' => $translates], 200);
    }

    /**
     * Get main crm info
     *
     * @api
     * @Rest\Get("api/auth_user")
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAuthUser()
    {
        $user = \Auth::user();
        return response()->json(['authUser' => User::with('roles')->find($user->id)], 200);
    }

}
