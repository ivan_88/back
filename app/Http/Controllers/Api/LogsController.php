<?php
/**
 * Logs controller
 *
 * @package App\Http\Controllers\Api
 * @author Ivan Dovhai <ivan_dovhai@i.ua>
 * @version v.1.0 (24/11/2017)
 */
namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;
use App\Content;
use App\ContentLog;
use App\User;

/**
 * Class LogsController
 */
class LogsController extends Controller
{
    /**
     * Count records in list
     *
     * @var int
     */
    private $perPage = 20;

    /**
     * Get logs events for one customer
     *
     * @api
     * @Rest\Get("api/logs/by_customer/{id}")
     * @param $request \Illuminate\Http\Request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLogByCustomer(Request $request, $id)
    {
        $customer = Content::find($id);
        if (!$customer) {
            return response()->json(['errors' => ['404']], 404);
        }

        $logs = ContentLog::with('user')
            ->where('content_id', '=', $id);

        if (isset($request->excel) && $request->excel == 1) {
            $logs = $logs->get();
            $this->makeExcel(
                $logs,
                ContentLog::getActions(),
                'logs.log_by_customer_table', 'Logs:_' . $customer->firstname . ' ' . $customer->lastname
            );
        } else {
            $logs = $logs->paginate($this->perPage);
            return response()->json(['log' => $logs, 'customer' => $customer, 'actions' => ContentLog::getActions()], 200);
        }
    }

    /**
     * View logs for one user
     *
     * @param $request \Illuminate\Http\Request
     * @param $id
     * @api
     * @Rest\Get("api/logs/by_user/{id}")
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLogByUser(Request $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return response()->json(['errors' => ['404']], 404);
        }
        $logs = ContentLog::where('user_id', '=', $id)
            ->with(['customer' => function ($query) {
                $query->select('firstname', 'lastname', 'id', 'rejection_reason', 'cause_of_recovery');
            }]);

        if (isset($request->excel) && $request->excel == 1) {
            $logs = $logs->get();
            $this->makeExcel($logs, ContentLog::getActions(), 'logs.log_by_user_table', 'Logs:_' . $user->name);
        } else {
            $logs = $logs->paginate($this->perPage);
            return response()->json(['log' => $logs, 'user' => $user, 'actions' => ContentLog::getActions()], 200);
        }
    }

    /**
     * View all logs with datetime filter
     *
     * Get parameters: 'start", "end" - optional, must be a date format "Y-m-d H:i:s"
     *
     * @param Request $request
     * @api
     * @Rest\Get("api/logs")
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGlobalLog(Request $request)
    {
        $logs = ContentLog::with(['user', 'customer' => function ($query) {
            $query->select('firstname', 'lastname', 'id', 'rejection_reason', 'cause_of_recovery');
        }]);
        if ($request->start) {
            $logs = $logs->where('created_at', '>=', $request->start);
        }
        if ($request->end) {
            $logs = $logs->where('created_at', '<=', $request->end);
        }

        if (isset($request->excel) && $request->excel == 1) {
            $logs = $logs->get();
            $this->makeExcel($logs, ContentLog::getActions(), 'logs.global_log_table', 'Reports: global');
        } else {
            $logs = $logs->paginate($this->perPage);
            return response()->json(['log' => $logs, 'actions' => ContentLog::getActions()], 200);
        }
    }

    /**
     * Make excel file
     *
     * @param Collection $data
     * @param array $actions
     * @param string $view
     * @param string $name
     * @return void
     */
    private function makeExcel(Collection $data, array $actions, string $view, string $name)
    {
        $excel = App::make('excel');
        $excel->create($name, function ($excel) use ($data, $actions, $view) {
            $excel->sheet('log', function ($sheet) use ($data, $actions, $view) {
                $sheet->loadView($view, ['log' => $data, 'actions' => $actions]);
            });
            $excel->store('xls')
                ->export('xls', ['Access-Control-Allow-Origin' => '*']);
        });
    }
}