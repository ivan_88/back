<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = ['firstname', 'lastname', 'phone', 'email', 'country_id', 'city', 'zip', 'address',
        'type', 'active', 'user_id', 'project_id', 'interest_id', 'source_id', 'rejection_reason', 'cause_of_recovery',
        'contact_by', 'app'];

    const LEAD = 1;
    const COM = 2;
    const NEG = 3;
    const PRO = 4;

    const LEAD_TO_COM = 1;
    const COM_TO_NEG = 2;
    const NEG_TO_PRO = 3;
    const PRO_TO_NEG = 4;
    const NEG_TO_COM = 5;
    const COM_TO_LEAD = 6;

    const VISIT = 1;
    const PHONE = 2;
    const MESSAGE = 3;

    const ARCHIVE = 0;
    const ACTIVE = 1;

    const APP_BROKERS = 1;
    const APP_SALES = 2;
    const APP_PARTNERS = 3;

    private static $apps = ['brokers' => self::APP_BROKERS, 'sales' => self::APP_SALES, 'partners' => self::APP_PARTNERS];

    public static function getApp(string $appLiteral) : int
    {
        if (array_key_exists($appLiteral, self::$apps)) {
            return self::$apps[$appLiteral];
        }
        return self::APP_BROKERS;
    }

    /**
     * Types of customers
     * @var array
     */
    private static $types = [
    	self::LEAD => 'LEAD',
    	self::COM => 'COM',
        self::NEG => 'NEG',
    	self::PRO => 'PRO'
    ];

    /**
     * Actions for moving
     * @var array
     */
    private static $actions = [
        self::LEAD_TO_COM => ['from' => self::LEAD, 'to' => self::COM],
        self::COM_TO_NEG => ['from' => self::COM, 'to' => self::NEG],
        self::NEG_TO_PRO => ['from' => self::NEG, 'to' => self::PRO],
        self::PRO_TO_NEG => ['from' => self::PRO, 'to' => self::NEG],
        self::NEG_TO_COM => ['from' => self::NEG, 'to' => self::COM],
        self::COM_TO_LEAD => ['from' => self::COM, 'to' => self::LEAD],
    ];

    /**
     * Actions for links
     * @var array
     */
    private static $actionsByType = [
        self::LEAD => ['forward' => self::LEAD_TO_COM, 'back' => null],
        self::COM => ['forward' => self::COM_TO_NEG, 'back' => self::COM_TO_LEAD],
        self::NEG => ['forward' => self::NEG_TO_PRO, 'back' => self::NEG_TO_COM],
        self::PRO => ['forward' => null, 'back' => self::PRO_TO_NEG],
    ];

    /**
     * List labels for "contact by" field
     * @var array
     */
    private static $contacts_by = [
        ['en' => 'Visit', 'uk' => 'Візит', 'id' => self::VISIT],
        ['en' => 'Phone', 'uk' => 'Дзвінок', 'id' => self::PHONE],
        ['en' => 'Message', 'uk' => 'Повідомлення', 'id' => self::MESSAGE]
    ];

    /**
     * Get actions for links
     * @return array
     */
    public static function getActionsByType()
    {
        return self::$actionsByType;
    }

    /**
     * List fields for search customers
     * @var array
     */
    private static $searchFields = [
        ['en' => 'In first name', 'uk' => 'В імені', 'field' => 'firstname'],
        ['en' => 'In last name', 'uk' => 'В прізвищі', 'field' => 'lastname'],
        ['en' => 'In phone', 'uk' => 'В телефоні', 'field' => 'phone']
    ];

    public static $searchFieldsNames = ['firstname', 'lastname', 'phone'];

    public static function getSearchFields($key = null)
    {
        if ($key) {
            return self::$searchFields[$key];
        }
        return self::$searchFields;
    }

    /**
     * Change type
     * @param $action
     * @return bool
     */
    public function move($action)
    {
        if ($this->type == self::$actions[$action]['from']) {
            $this->type = self::$actions[$action]['to'];
            $this->save();
            return true;
        }
        return false;
    }

    /**
     * Check does it exists type
     * @param int $key
     * @return bool
     */
    public static function checkType(int $key)
    {
    	return array_key_exists($key, self::$types);
    }

    /**
     * Return one type if $key transmitted, else return array types
     * @param null $key
     * @return array|mixed
     */
    public static function getTypes($key = null)
    {
    	if ($key && self::checkType($key)) {
    		return self::$types[$key];
    	}
    	return self::$types;
    }

    public static function getContactsBy($key = null)
    {
        if ($key && self::checkType($key)) {
            return self::$contacts_by[$key];
        }
        return self::$contacts_by;
    }

    public function country()
    {
    	return $this->belongsTo('App\Country');
    }

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function project()
    {
    	return $this->belongsTo('App\Project');
    }

    public function interest()
    {
        return $this->belongsTo('App\Interest');
    }

    public function source()
    {
        return $this->belongsTo('App\Source');
    }

    public function notes()
    {
        return $this->hasMany('App\Note');
    }

    /**
     * Validation rules, for update if transmitted $id, else rules for create
     * @param null|string $id
     * @param null|string $phone
     * @param null|string $email
     * @return array
     */
    public static function getRules($id = null, $phone = null, $email = null)
    {
        if($id) {
            return [
                'firstname' => 'required|max:32|string',
                'lastname' => 'required|max:32|string',
                'phone' => $email != '' ? 'max:13' : 'required|max:13|unique:contents,phone,' . $id . '|regex:~\d~',
                'email' => $phone != '' ? 'max:64' : 'required|max:64|unique:contents,email,' . $id . '|email',
                'country_id' => 'exists:countries,id',
                'city' => 'max:32',
                'address' => 'max:256',
                'interest_id' => 'required|integer|exists:interests,id',
                'source_id' => 'required|integer|exists:sources,id',
                'contact_by' => 'required|in:1,2,3'
            ];
        }
        return [
            'firstname' => 'required|max:32|string',
            'lastname' => 'required|max:32|string',
            'phone' => $email != '' ? 'max:13' : 'required|max:13|unique:contents,phone|regex:~\d~',
            'email' => $phone != '' ? 'max:64' : 'required|max:64|unique:contents,email|email',
            'country_id' => 'exists:countries,id',
            'city' => 'max:32',
            'address' => 'max:256',
            'interest_id' => 'required|integer|exists:interests,id',
            'source_id' => 'required|integer|exists:sources,id',
            'contact_by' => 'required|in:1,2,3',
            'project_id' => 'required|integer|exists:projects,id'
        ];
    }

    public static function getRulesForArchiveOrActive()
    {
        return [
            'reason' => 'required',
            'action' => 'required|in:0,1'
        ];
    } 

    /**
     * Get list customers with filters
     * @param \Illuminate\Http\Request $request
     * @param int $app
     * @param User $user
     * @param int $perPage
     */
    public static function getList(\Illuminate\Http\Request $request, int $app, User $user, int $perPage) 
    {
        if ($request->search) {
            $field = in_array($request->field, Content::$searchFieldsNames) ? $request->field : 'lastname';
            $contents = self::where($field, 'like', '%' . $request->search . '%')
                ->where('app', '=', $app);// Костыль
        } else {
            $contents = self::where('type', '=', $request->type)
                ->where('active', '=', $request->active)
                ->where('project_id', '=', $request->project)
                ->where('app', '=', $app);// Костыль
        }

        if (!$user->hasRole('manager') && !$user->hasRole('admin')) {
            $contents = $contents->where('user_id', '=', $user->id);
        }

        if ($request->interest_id) {
            $contents = $contents->where('interest_id', '=', $request->interest_id);
        }
        if ($request->source_id) {
            $contents = $contents->where('source_id', '=', $request->source_id);
        }

        if ($request->order_by) {
            $contents = $contents->orderBy($request->order_by, $request->order_direction);
        }

        return $contents->with(['user', 'interest', 'source'])->paginate($perPage);
    }

    /**
     * Get one customer
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return Content
     */
    public static function getOne($id)
    {
        return self::with([
            'country' => function ($query) {
                $query->select('id', 'full_name');
            },
            'interest',
            'project',
            'source',
            'user' => function ($query) {
                $query->select('id', 'name');
            }
        ])
            ->where('id', '=', $id)
            ->first();
    }
}
