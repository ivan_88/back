<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Auth;
use App\Content;

class ContentHelper
{
    /**
     * Determining user permission to move customer
     * @param integer $action
     * @return bool
     */
    public static function canUserMove($customerId, $action) : bool
    {
        $user = Auth::user();
        return ($user->hasRole('admin') || $user->hasRole('manager'))
            || (in_array($action, [Content::LEAD_TO_COM, Content::COM_TO_LEAD]) && $user->id == $customerId);
    }
}