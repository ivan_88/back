<?php

namespace App\Helpers;

use App\Source;
use Illuminate\Support\Facades\Cache;
use App\Country;
use App\Project;
use App\Interest;

class CacheHelper
{
	public static function getCountries()
	{
		if (Cache::get('countries')) {
            $countries = Cache::get('countries');
        } else {
            $countries = Country::select('id', 'full_name')->get();
            Cache::put('countries', $countries, 60 * 24);
        }

        return $countries;
	}

	public static function getProjects()
    {
        if (Cache::has('projects')) {
            $projects = Cache::get('projects');

        } else {
            $projects = Project::all();
            Cache::put('projects', $projects, 60 * 24);
        }

        return $projects;
    }

    public static function getInterests($projectId = null)
    {
        if ($projectId) {
            if (Cache::has('interests_' . $projectId)) {
                $interests = Cache::get('interests_' . $projectId);
            } else {
                $interests = Interest::where('project_id', '=', $projectId)->get();
                Cache::put('interests_' . $projectId, $interests, 60 * 24);
            }
        } else {
            if (Cache::has('interests')) {
                $interests = Cache::get('interests');
            } else {
                $interests = Interest::all();
                Cache::put('interests', $interests, 60 * 24);
            }
        }

        return $interests;
    }

    public static function getSources()
    {
        if (Cache::get('sources')) {
            $sources = Cache::get('sources');
        } else {
            $sources = Source::all();
            Cache::put('sources', $sources, 60 * 24);
        }

        return $sources;
    }
}