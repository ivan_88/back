<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    protected $fillable = ['text', 'content_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getList(int $id, int $perPage)
    {
    	return self::with('user')->where('content_id', '=', $id)->paginate($perPage);
    }
}
