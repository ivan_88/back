<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    const ADMIN = 1;
    const MANAGER = 2;
    const BROKER = 3;
    const SALES = 4;
    const PARTNER = 5;
}
