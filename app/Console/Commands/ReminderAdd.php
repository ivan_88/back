<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Content;
use App\Reminder;
use Illuminate\Support\Facades\DB;

class ReminderAdd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'reminder:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Adding reminders for customers without activities for last week';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $thisDateTime = new \DateTime();
        $thisDateTime->modify('-1 week');

        $contents = Content::where('updated_at', '<=', $thisDateTime)->get();
        $reminders = [];
        foreach ($contents as $content) {
            $reminders[] = [
                'user_id' => $content->user_id,
                'content_id' => $content->id,
                'note' => trans('interface.week_no_activity'),
                'assigned_to' => date('Y-n-d H:i:s'),
                'created_at' => date('Y-n-d H:i:s'),
                'updated_at' => date('Y-n-d H:i:s')
            ];
        }

        DB::table('reminders')->insert($reminders);
        Content::where('updated_at', '<=', $thisDateTime)->update(['updated_at' => date('Y-n-d H:i:s')]);
    }
}
