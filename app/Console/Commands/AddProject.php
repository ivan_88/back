<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Project;

class AddProject extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:add {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create project';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $project = Project::create(['name' => $this->argument('name')]);
        if ($project) {
            $this->info('Project ' . $this->argument('name') . ' added');
        } else {
            $this->error('Error!');
        }
    }
}
