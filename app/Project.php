<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $fillable = ['name_en', 'name_uk'];
    protected $dates = ['deleted_at'];

    /**
     * Validation rules
     * @var array
     */
    public static $rules = ['name_en' => 'required', 'name_uk' => 'required'];
}
