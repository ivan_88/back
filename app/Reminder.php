<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    protected $fillable = ['user_id', 'content_id', 'note', 'assigned_to', 'show', 'done'];

    public function content()
    {
        return $this->belongsTo('App\Content');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Validation rules
     * @var array
     */
    public static $rules = [
        'content_id' => 'required|integer|exists:contents,id',
        'note' => 'required',
        'assigned_to' => 'required|date'
    ];

    public static function getList(int $userId, \Illuminate\Http\Request $request, $today, $tomorrow, $perPage)
    {
        $reminders = self::with('content')
            ->where('user_id', '=', $userId)
            ->where('done', '=', $request->executed);
        if ($request->today && $request->today == 1) {
            $reminders = $reminders ->where('assigned_to', '>=', $today)
                ->where('assigned_to', '<=', $tomorrow);
        }
        return $reminders->paginate($perPage);
    }

    public static function getTodayRemindersCount(int $userId, $today, $tomorrow) 
    {
        return Reminder::where('user_id', '=', $userId)
            ->where('done', '=', 0)
            ->where('assigned_to', '>=', $today)
            ->where('assigned_to', '<=', $tomorrow)
            ->get();
    }
}
