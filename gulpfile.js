'use strict';

const gulp = require('gulp');
const elixir = require('laravel-elixir');


elixir(function (mix) {
    mix.styles( '../../../bower_components/bootstrap/dist/css/bootstrap.min.css');
    mix.styles( '../../../bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
    mix.styles( '/main.css');
    mix.scripts('main.js', 'public/js/main.js');
    mix.scripts('contents.js', 'public/js/contents.js');
    mix.scripts('../../../bower_components/jquery/dist/jquery.min.js', 'public/js/jquery.min.js');
    mix.scripts('../../../bower_components/moment/min/moment.min.js', 'public/js/moment.min.js');
    mix.scripts('../../../bower_components/bootstrap/dist/js/bootstrap.min.js', 'public/js/bootstrap.min.js');
    mix.scripts('../../../bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js', 'public/js/bootstrap-datetimepicker.min.js');
});
